package com.luke.xiaotiaowa.component;

import com.luke.xiaotiaowa.events.XtwEvents;
import com.luke.xiaotiaowa.utils.HttpResponseUtil;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 当访问接口没有权限时，自定义的返回结果
 * 
 * @author xiaotiaowa
 * @create 2023/3/24
 */
public class RestfulAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {
        HttpResponseUtil.write(response, XtwEvents.FORBIDDEN.get());
    }
}
