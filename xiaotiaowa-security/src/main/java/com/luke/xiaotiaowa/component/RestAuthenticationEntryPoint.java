package com.luke.xiaotiaowa.component;

import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONUtil;
import com.luke.xiaotiaowa.events.XtwEvents;
import com.luke.xiaotiaowa.utils.HttpResponseUtil;
import org.apache.tomcat.util.http.ResponseUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义未登录或者token失效时的返回结果
 * 
 * @author xiaotiaowa
 * @create 2023/3/24
 */
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
    
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        HttpResponseUtil.write(response, XtwEvents.UNAUTHORIZED.get());
    }
}
