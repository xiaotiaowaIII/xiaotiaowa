package com.luke.xiaotiaowa;

import org.springframework.security.access.ConfigAttribute;

import java.util.Map;

/**
 * 动态权限相关业务接口
 * 
 * @author xiaotiaowa
 * @create 2023/3/24
 */
public interface DynamicSecurityService {

    /**
     * 加载资源ANT通配符和资源对应MAP
     * @return map
     */
    Map<String, ConfigAttribute> loadDataSource();
}
