package com.luke.xiaotiaowa.utils;

import cn.hutool.json.JSONUtil;
import com.luke.xiaotiaowa.events.XtwEvent;
import com.luke.xiaotiaowa.events.XtwEvents;
import com.luke.xiaotiaowa.resp.ResponseVo;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author xiaotiaowa
 * @create 2023/3/24
 */
public class HttpResponseUtil {
    
    private HttpResponseUtil() {}
    
    public static void write(HttpServletResponse response, XtwEvent event) throws IOException {
        var responseVo = ResponseVo.builder()
                .code(event.getCode())
                .message(event.getMessage())
                .build();
        response.setStatus(event.getCode());
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Cache-Control","no-cache");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(JSONUtil.parse(responseVo));
        response.getWriter().flush();
    }
}
