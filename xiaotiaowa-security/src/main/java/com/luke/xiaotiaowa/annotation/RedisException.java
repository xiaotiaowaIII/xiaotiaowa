package com.luke.xiaotiaowa.annotation;

import java.lang.annotation.*;

/**
 * 自定义redis缓存异常注解
 * 
 * @author xiaotiaowa
 * @create 2023/4/4
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RedisException {
}
