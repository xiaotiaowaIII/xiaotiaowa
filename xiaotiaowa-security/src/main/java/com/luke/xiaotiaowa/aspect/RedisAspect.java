package com.luke.xiaotiaowa.aspect;

import com.luke.xiaotiaowa.annotation.RedisException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * redis缓存切面。防止redis宕机影响正常业务
 * 
 * @author xiaotiaowa
 * @create 2023/4/4
 */
@Slf4j
@Aspect
@Component
@Order(2)
public class RedisAspect {
    
    @Pointcut("execution(public * com.*.*.modules.*.cache.*CacheService.*(..))")
    public void redisAspect() {
    }

    @Around("redisAspect()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        var signature = joinPoint.getSignature();
        var methodSignature = (MethodSignature) signature;
        var method = methodSignature.getMethod();
        Object result = null;
        try {
            result = joinPoint.proceed();
        } catch (Throwable throwable) {
            //有CacheException注解的方法需要抛出异常
            if (method.isAnnotationPresent(RedisException.class)) {
                throw throwable;
            } else {
                log.error(throwable.getMessage());
            }
        }
        return result;
    }
}
