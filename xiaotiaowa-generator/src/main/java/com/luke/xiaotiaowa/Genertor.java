package com.luke.xiaotiaowa;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.luke.xiaotiaowa.common.BaseEntity;

import java.util.List;

/**
 * @author xiaotiaowa
 * @create 2023/3/22
 */
public class Genertor {

    /**
     * 数据源配置
     */
    private static final DataSourceConfig DATA_SOURCE_CONFIG = new DataSourceConfig
            .Builder("jdbc:mysql://localhost:3306/xiaotiaowa?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai",
            "root", "123456")
            .build();

    /**
     * 策略配置
     */
    public static StrategyConfig.Builder strategyConfig() {
        // 设置需要生成的表名
        return new StrategyConfig.Builder()
                .addInclude(List.of("mms_account"));
    }

    /**
     * 全局配置
     */
    protected static GlobalConfig.Builder globalConfig() {
        return new GlobalConfig.Builder().author("xiaotiaowa");
    }

    /**
     * 包配置
     */
    protected static PackageConfig.Builder packageConfig() {
        return new PackageConfig.Builder();
    }


    /**
     * 执行 run
     */
    public static void main(String[] args) {
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig().entityBuilder()
                .enableLombok()
                .superClass(BaseEntity.class)
                // 基于数据库字段
                .versionColumnName("version")
                // 基于模型属性
                .versionPropertyName("version")
                // 基于数据库字段
                .logicDeleteColumnName("deleted")
                // 基于模型属性
                .logicDeletePropertyName("deleteFlag")
                // 基于数据库字段
                .addIgnoreColumns("create_by", "create_time", "lst_mod_by", "lst_mod_time")
                // 重命名模板生成的文件名称
                .formatFileName("%sEntity")
                .mapperBuilder().formatMapperFileName("%sMapper").formatXmlFileName("%sMapper")
                .controllerBuilder().formatFileName("%sController")
                .serviceBuilder().formatServiceFileName("%sService").formatServiceImplFileName("%sServiceImpl")
                .build());
        generator.global(globalConfig()
                .enableSwagger()
                .build());
        generator.packageInfo(packageConfig()
                .parent("com.luke.xiaotiaowa.modules")
                .moduleName("ums").build());
        generator.execute();
    }
}
