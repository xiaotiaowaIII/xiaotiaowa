package com.luke.xiaotiaowa.context;

import cn.hutool.core.map.MapUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xiaotiaowa
 * @create 2023/3/23
 */
public class MainThreadLocal {

    public static final String M_PAGE = "mpage";
    public static final String IS_PAGE = "is_page";
    
    private MainThreadLocal() {
        throw new IllegalStateException("Utility class");
    }

    private static final ThreadLocal<HashMap<Object, Object>> CURRENCY = ThreadLocal.withInitial(() -> {
        var map = MapUtil.newHashMap();
        map.put(IS_PAGE, false);
        return map;
    });

    public static void set(String k, Object v) {
        CURRENCY.get().put(k, v);
    }

    public static Object get(String k) {
        return ((Map<?, ?>)CURRENCY.get()).get(k);
    }
    
    public static boolean isPage() {
        return (Boolean) get(IS_PAGE);
    }
    public static void setPage(Object obj) {
        set(IS_PAGE, true);
        set(M_PAGE, obj);
    }
    public static Object getPage() {
       return get(M_PAGE);
    }
    
    public static void clean() {
        CURRENCY.remove();
    }
}
