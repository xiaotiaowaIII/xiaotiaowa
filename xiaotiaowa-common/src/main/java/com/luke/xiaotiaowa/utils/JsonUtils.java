package com.luke.xiaotiaowa.utils;

import com.alibaba.fastjson.JSON;
import org.springframework.boot.SpringApplication;

import java.math.BigDecimal;

/**
 * @author xiaotiaowa
 * @create 2023/3/29
 */
public class JsonUtils {

    public static void main(String[] args) {
        BigDecimal flag = BigDecimal.valueOf((float) 100 / 300).setScale(2, BigDecimal.ROUND_HALF_UP);
        System.out.println(flag);
    }
    
    private JsonUtils(){}

    public static  <T> T parseObject(String text, Class<T> clazz) {
        return JSON.parseObject(text, clazz);
    }

    public static String toJsonString(Object javaObject) {
        return JSON.toJSONString(javaObject);
    }

    public static byte[] toJsonBytes(Object javaObject) {
        return JSON.toJSONBytes(javaObject);
    }
}
