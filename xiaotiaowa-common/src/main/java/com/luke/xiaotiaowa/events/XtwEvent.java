package com.luke.xiaotiaowa.events;

/**
 * @author xiaotiaowa
 */
public class XtwEvent extends AbstractException {
    private static final long serialVersionUID = 8429332849404585853L;
    
    public XtwEvent(int code, String message, Object... params) {
        this.message(message);
        this.setCode(code);
        this.setParams(params);
    }

    public XtwEvent(int code, Throwable e) {
        super(e);
        this.setCode(code);
    }

    public XtwEvent(Object data) {
        this.setCode(200);
        this.setData(data);
    }

    public XtwEvent message(String message) {
        this.setMessage(message);
        return this;
    }

    public XtwEvent params(Object... params) {
        this.setParams(params);
        return this;
    }

    public XtwEvent cause(Throwable e) {
        this.setStackTrace(e.getStackTrace());
        return this;
    }
    
    public void ifTrueThrow(boolean expression) {
        if (expression) {
            throw this;
        }
    }
    
    public void ifFalseThrow(boolean expression) {
        ifTrueThrow(!expression);
    }
}
