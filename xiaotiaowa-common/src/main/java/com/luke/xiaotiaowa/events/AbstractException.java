package com.luke.xiaotiaowa.events;

import lombok.Getter;
import lombok.Setter;

/**
 * @author xiaotiaowa
 */
@Getter
@Setter
public class AbstractException extends RuntimeException {
    private static final long serialVersionUID = -5117585549955893073L;
    private Integer code;
    private String message;
    private Object data;
    private Object[] params;

    public AbstractException() {
    }

    public AbstractException(String message) {
        super(message);
    }

    public AbstractException(Throwable e) {
        super(e);
    }
}
