package com.luke.xiaotiaowa.events;

import lombok.Getter;

import java.util.function.Supplier;

/**
 * @author xiaotiaowa
 */
@Getter
public enum XtwEvents implements Supplier<XtwEvent>{
    /**
     * 响应码和异常信息
     */
    OK(200, "success"),
    BAD_REQUEST(400),
    UNAUTHORIZED(401, "Unauthorized"),
    FORBIDDEN(403, "Forbidden"),
    NOT_FOUND(404, "Not Found"),
    METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
    INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
    OTHERS(503, "Service Unavailable");

    private final Integer status;
    private String message;

    XtwEvents(Integer status) {
        this.status = status;
    }

    XtwEvents(int status, String message) {
        this.status = status;
        this.message = message;
    }
    
    @Override
    public XtwEvent get() {
        return new XtwEvent(this.status, this.message);
    }

}
