package com.luke.xiaotiaowa.properties;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;

/**
 * redis相关配置信息
 * 
 * @author xiaotiaowa
 * @create 2023/3/29
 */
@Getter
@Setter
public class XtwRedisProperties {

    /**
     * redis数据库
     */
    private String database;

    /**
     * redis 的一些key
     */
    private HashMap<String, String> key;
}
