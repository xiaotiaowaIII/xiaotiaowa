package com.luke.xiaotiaowa.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 配置
 * 
 * @author xiaotiaowa
 * @create 2023/3/29
 */
@Component
@ConfigurationProperties(prefix = "com.luke")
@Getter
@Setter
public class XtwProperties {
    
    private XtwRedisProperties redis = new XtwRedisProperties();
}
