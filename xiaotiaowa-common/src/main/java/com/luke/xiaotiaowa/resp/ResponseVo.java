package com.luke.xiaotiaowa.resp;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 接口返回结果封装类
 * 
 * @author xiaotiaowa
 * @create 2023/3/23
 */
@Getter
@Setter
@Builder
public class ResponseVo {

    /**
     * 状态码
     */
    private long code;
    /**
     * 提示信息
     */
    private String message;
    /**
     * 数据封装
     */
    private Object data;

    /**
     * 分页信息
     */
    private Object page;
}
