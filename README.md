# `XTW`

## 前言
`XTW`供学习java相关技术用

## 项目介绍
`XTW`是一套社交系统，包括前台系统及后台管理系统，前台系统包括首页门户、消息通知、个人主页、文章搜索、搜索记录、浏览记录、文章推荐、热门话题排行榜、发布文章、帮助及客户中心等模块，后台系统包括文章管理、用户管理、话题管理、统计报表、文章评审、权限管理、设置等模块

## 组织结构
```lua=
xiaotiaowa
├── xiaotiaowa-common -- 工具类及通用代码
├── xiaotiaowa-generator -- MyBatisGenerator生成的数据库操作代码
├── xiaotiaowa-security -- SpringSecurity封装公用模块
├── xiaotiaowa-admin -- 平台管理系统接口
├── xiaotiaowa-modules -- 业务逻辑处理代码
├── xiaotiaowa-search -- 基于Elasticsearch的消息搜寻系统
├── xtw-admin-web -- 基于vue3的后天管理前端界面
```

## 技术框架&工具
### 后端技术
| 技术                   | 版本    | 说明                |
| ---------------------- | ------- | ------------------- |
| OpenJDK                | 11.0.2  | jdk                 |
| SpringBoot             | 2.7.9   | Web应用开发框架     |
| SpringSecurity         | 2.7.9   | 认证和授权框架      |
| MyBatis-plus           | 3.5.2   | Mybatis的增强工具   |
| MyBatis-plus Generator | 3.5.2   | 数据层代码生成器    |
| Elasticsearch          | 8.6.2   | 搜索引擎            |
| MongoDB                | 5.0.15  | NoSql数据库         |
| Redis                  | 7.0.9   | 内存数据缓存        |
| Mysql                  | 8.0.32  | 数据库              |
| Flyway                 | 7.9.1   | 数据库的管理和迁移  |
| Druid                  | 1.2.14  | 数据库连接池        |
| JWT                    | 0.9.1   | JWT登录支持         |
| Lombok                 | 1.18.26 | Java语言增强库      |
| Hutool                 | 5.8.9   | Java工具类库        |
| Hibernator-Validator   | 6.1.5.Final | 验证框架            |
| Swagger-UI             | 3.0.0   | API文档生成工具     |
| OSS                    | 3.15.1  | 对象存储            |
| MinIO                  |         | 对象存储            |
| Mybatis-plus-generator | 3.5.2   | 代码生成器          |

### 前端技术
| 技术           | 版本       | 说明    |
|--------------|----------|-------|
| Vue          | 3        | 前端框架  |
| element-plus | 2.2.17   | 前端UI  |
| axios        | 0.21.1   | 网络请求库 |
| echarts      | 5.4.2    | 图表库   |


## 架构编
### 后端编
* [SpringBoot+MySql+Mybatis-plus+Flyway搭建基本骨架](document/md/SpringBoot+MySql+Mybatis-plus+Flyway搭建基本骨架.md)
* [整合Swagger3实现在线API文档](document/md/整合Swagger3实现在线API文档.md)
* [后端接口设计：参数校验、全局异常处理、统一响应格式](document/md/后端接口设计：参数校验、全局异常处理、统一响应格式.md)
* [整合SpringSecurity和JWT, 用户认证和授权](document/md/整合SpringSecurity和JWT,用户认证和授权.md)
* [整合Redis实现缓存功能](document/md/整合Redis实现缓存功能.md)
* [整合MongoDB+AOP，实现日志的记录](document/md/整合MongoDB+AOP，实现日志的记录.md)
* [整合阿里云OSS，实现文件上传](document/md/整合阿里云OSS，实现文件上传.md)
* [整合SonarQube源代码检测工具](document/md/整合SonarQube源代码检测工具.md)
* [自定义注解+AOP+Redis实现防重复提交](document/md/自定义注解+AOP+Redis实现防重复提交.md)
### 前端编
