export default {
  development: {
    baseUrl: '/api' // 测试接口域名
  },
  beta: {
    baseUrl: '//localhost:8080' // 测试接口域名
  },
  release: {
    baseUrl: '//localhost:8080' // 正式接口域名
  }
}
