import axios from 'axios'
import {getToken} from './auth'
import config from '~/config'

// 这边由于后端没有区分测试和正式，姑且都写成一个接口。
axios.defaults.baseURL = config[import.meta.env.MODE].baseUrl
// 携带 cookie，对目前的项目没有什么作用，因为我们是 token 鉴权
axios.defaults.withCredentials = true
axios.defaults.headers['Authorization'] = 'Bearer ' + getToken() || ''
// 默认 post 请求，使用 application/json 形式
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8'

axios.interceptors.response.use(res => {
    return res
})
//结果拦截
axios.interceptors.response.use(
    (res) => {
        return res;
    },
    err => {
        return Promise.reject(err);
    }
);

export default axios
