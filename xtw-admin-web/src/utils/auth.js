import { localGet, localSet, localRemove } from '@/utils'
const tokenKey = "token";
const infoKty = "admin_info"

export function getToken() {
    return localGet(tokenKey);
}

export function setToken(token) {
    return localSet(tokenKey, token);
}


export function removeToken() {
    return localRemove(tokenKey)
}

export function setInfo(info) {
    return localSet(infoKty, info);
}

export function getInfo() {
    return localGet(infoKty);
}


export function removeInfo() {
    return localRemove(infoKty)
}
