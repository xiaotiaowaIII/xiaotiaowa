import { createRouter, createWebHashHistory } from 'vue-router'
import Layout from '@/layout/Index.vue'
export const constantRoutes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login/Login.vue')
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import( '../views/dashboard/index.vue'),
        name: 'Dashboard',
        meta: {
          title: '首页',
          affix: true
        }
      }
    ]
  },
  {
    path: '/pms',
    name: 'pms',
    component: Layout,
    redirect: 'noredirect',
    meta: {title: '帖子模块'},
    children: [
      {
        path: '/posts',
        name: 'posts',
        component: () => import('@/views/error-page/404.vue'),
        meta: { title : '帖子搜索'}
      },
      {
        path: '/posts',
        name: 'posts',
        component: () => import('@/views/error-page/404.vue'),
        meta: { title : '帖子审核'}
      }
    ]
  },
  {
    path: '/mms',
    name: 'mms',
    component: Layout,
    redirect: 'noredirect',
    meta: {title: '用户模块'},
    children: [
      {
        path: '/account',
        name: 'account',
        component: () => import('@/views/mms/account/index.vue'),
        meta: { title : '用户信息'}
      },
      {
        path: '/account/analysis',
        name: 'analysis',
        component: () => import('@/views/error-page/404.vue'),
        meta: { title : '用户分析'}
      }
    ]
  }, 
  {
    path: '/oms',
    name: 'oms',
    component: Layout,
    redirect: 'noredirect',
    meta: {title: '运营模块'},
    children: [
      
    ]
  },
  {
    path: '/ums',
    name: 'ums',
    component: Layout,
    redirect: 'noredirect',
    meta: { title : '权限模块'},
    children: [
      {
        path: 'admin',
        name: 'admin',
        component: () => import('@/views/ums/admin/index.vue'),
        meta: { title : '管理员'},
      },
      {
        path: 'role',
        name: 'role',
        component: () => import('@/views/ums/role/index.vue'),
        meta: { title : '角色管理'},
      },
      {
        path: 'perm',
        name: 'perm',
        component: () => import('@/views/ums/perm/index.vue'),
        meta: { title : '权限管理'},
      },
      {
        path: 'permCategory',
        name: 'permCategory',
        component: () => import('@/views/error-page/404.vue'),
        alwaysShow: false
      }
    ]
  },
  {
    path: '/sys',
    name: 'sys',
    component: Layout,
    redirect: 'noredirect',
    meta: { title : '系统管理'},
    children: [
      {
        path: 'adminAccessLog',
        name: 'adminAccessLog',
        component: () => import('@/views/system/adminAccessLog/index.vue'),
        meta: { title : '后台访问接口日志'}
      },
      {
        path: 'properties',
        name: 'properties',
        component: () => import('@/views/error-page/404.vue'),
        meta: { title : '参数设置'}
      },
    ]
  },
  {
    path: '/error',
    component: Layout,
    redirect: 'noredirect',
    alwaysShow: false,
    hidden: true,
    children: [
      {
        path: '403',
        component: () => import('@/views/error-page/403.vue')
      },
      {
        path: '404',
        component: () => import('@/views/error-page/404.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes: constantRoutes
})

export default router
