export const apis = {}
apis.LOGIN = "/auth";
apis.LOGON_INFO = "/auth/info"
// ums 权限管理
apis.UMS_ADMIN = "/ums/admin"
apis.ONE_UMS_ADMIN = "/ums/{id}/admin"
apis.ACTIVATE_UMS_ADMIN = "/ums/{id}/admin/activate"
apis.SUSPENDED_UMS_ADMIN = "/ums/{id}/admin/suspended"
apis.ASSIGN_ROLES = "/ums/{id}/admin/assignRoles"

apis.ALL_ROLE = "/ums/roles"
apis.ADMIN_ROLES = "/ums/roles/{adminId}/admin"

apis.UMS_ROLE = "/ums/role"
apis.ONE_UMS_ROLE = "/ums/{id}/role"
apis.ACTIVATE_UMS_ROLE = "/ums/{id}/role/activate"
apis.SUSPENDED_UMS_ROLE = "/ums/{id}/role/suspended" 

apis.ROLE_PERMS="/ums/perms_category/{roleId}/role"
apis.ASSIGN_PERM="/ums/{id}/role/assignPerms"

apis.ALL_PERM_CATEGORY = "/ums/perm_category/listAll"
apis.PERM_CATEGORY = "/ums/perm_category"
apis.ONE_PERM_CATEGORY = "/ums/{id}/perm_category"

apis.PERM = "/ums/perm"
apis.ONE_PERM = "/ums/{id}/perm"

// system系统管理
apis.ADMIN_ACCESS_LOG = "/system/admin/accessLog"

// mms 用户模块
apis.ACCOUNT = "/mms/account"
apis.ACTIVATE_ACCOUNT = "/mms/{id}/account/activate"
apis.SUSPENDED_ACCOUNT = "/mms/{id}/account/suspended"

// 文件操作
apis.UPLOAD_FILE = "/file/upload"
