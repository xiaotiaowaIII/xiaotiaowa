package com.luke.xiaotiaowa.repository;

import com.luke.xiaotiaowa.domain.EsTestProduct;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * es 商品操作类
 * 
 * @author xiaotiaowa
 */
public interface EsTestProductRepository extends ElasticsearchRepository<EsTestProduct, Long> {
    
    
}
