package com.luke.xiaotiaowa.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * es索引--商品
 *
 * @author xiaotiaowa
 */
//@Document(indexName = "test_product")
@Data
@EqualsAndHashCode
@Setting(shards = 1, replicas = 0)
public class EsTestProduct implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String name;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String subTitle;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String keywords;
    private BigDecimal originalPrice;
    private Integer stock;
    private Integer lowStock;
    private String unit;
    private BigDecimal weight;
    private String note;
    private String detailTitle;
    private String detailDesc;
    @Field(type = FieldType.Keyword)
    private String brandName;
    @Field(type = FieldType.Keyword)
    private String productCategoryName;
}
