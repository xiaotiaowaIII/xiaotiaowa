package com.luke.xiaotiaowa;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * 应用启动入口
 * 
 * @author xiaotiaowa
 * @create 2023/3/22
 */
@Slf4j
@EnableOpenApi
@SpringBootApplication
public class XtwSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(XtwSearchApplication.class, args);
    }
    
}
