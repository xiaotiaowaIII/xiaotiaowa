package com.luke.xiaotiaowa.aspect.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 
 * 防重复提交注解
 * 
 * @author xiaotiaowa
 */
@Inherited
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatSubmit {
    
    /**
     * 锁定时间，默认3000毫秒(3秒)
     */
    int interval() default 3000;

    /**
     * 锁定时间单位，默认毫秒
     */
    TimeUnit timeUnit() default TimeUnit.MILLISECONDS;

    /**
     * 提示信息
     */
    String message() default "频繁操作，请稍后再试！";
    
}
