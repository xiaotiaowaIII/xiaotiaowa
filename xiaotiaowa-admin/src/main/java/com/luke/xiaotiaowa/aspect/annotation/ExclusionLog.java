package com.luke.xiaotiaowa.aspect.annotation;

import java.lang.annotation.*;

/**
 * 排除记录日志
 * @author xiaotiaowa
 * @create 2023/3/30
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ExclusionLog {
}
