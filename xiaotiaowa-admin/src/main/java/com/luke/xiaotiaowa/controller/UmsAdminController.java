package com.luke.xiaotiaowa.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luke.xiaotiaowa.aspect.annotation.RepeatSubmit;
import com.luke.xiaotiaowa.context.MainThreadLocal;
import com.luke.xiaotiaowa.controller.switches.UmsAdminSwitches;
import com.luke.xiaotiaowa.controller.switches.vo.UmsAdminSwitchVo;
import com.luke.xiaotiaowa.modules.ums.entity.UmsAdminEntity;
import com.luke.xiaotiaowa.modules.ums.service.UmsAdminService;
import com.luke.xiaotiaowa.valid.IGroups;
import com.luke.xiaotiaowa.vo.ums.AssignRolesVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.groups.Default;
import java.util.List;

/**
 * <p>
 * 后台管理员表 前端控制器
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-22
 */
@Api(tags = "后台用户管理")
@RestController
@RequestMapping("ums")
public class UmsAdminController {
    
    @Resource
    private UmsAdminService umsAdminService;
    
    @ApiOperation("根据用户名或姓名分页获取后台用户列表")
    @GetMapping("admin")
    public List<UmsAdminEntity> umsAdminPage(@ApiParam("搜索条件") @RequestParam(value = "keyword", required = false) String keyword,
                                             @ApiParam("每页数量") @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                             @ApiParam("页码") @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        var page = new Page<UmsAdminEntity>(pageNum, pageSize, true);
        MainThreadLocal.setPage(page);
        return umsAdminService.selectUmsAdmin(page, keyword);
    }
    
    @ApiOperation("通过主键查询单条后台用户")
    @GetMapping("/{id}/admin")
    public UmsAdminEntity umsAdmin(@ApiParam("主键") @PathVariable long id) {
        return umsAdminService.selectUmsAdmin(id);
    }
    
    @ApiOperation("新增后台用户")
    @RepeatSubmit
    @PostMapping("admin")
    public UmsAdminEntity addUmsAdmin(@Validated({Default.class, IGroups.ADD.class}) @RequestBody UmsAdminEntity umsAdmin) {
        return umsAdminService.addUmsAdmin(umsAdmin);
    }
    
    @ApiOperation("编辑后台用户")
    @RepeatSubmit
    @PutMapping("/{id}/admin")
    public UmsAdminEntity modUmsAdmin(@ApiParam("主键") @PathVariable long id ,
                                      @Validated({Default.class, IGroups.MOD.class})@RequestBody UmsAdminEntity umsAdmin) {
        return umsAdminService.modUmsAdmin(id, umsAdmin);
    }
    
    @ApiOperation("删除后台用户")
    @RepeatSubmit
    @DeleteMapping("/{id}/admin")
    public void delUmsAdmin(@ApiParam("主键") @PathVariable long id) {
        umsAdminService.delUmsAdmin(id);
    }
    
    @ApiOperation("启用或禁用后台用户")
    @RepeatSubmit
    @PutMapping("/{id}/admin/{opt:activate|suspended}")
    public void activateOrSuspendedAdmin(@ApiParam("主键") @PathVariable long id, @ApiParam("activate or suspended") @PathVariable String opt) {
        final var strategy = UmsAdminSwitches.strategy(opt);
        var vo = UmsAdminSwitchVo.builder().build();
        vo.setId(id);
        vo.setAdminService(umsAdminService);
        assert strategy != null;
        strategy.accept(vo);
    }
    
    @ApiOperation("分配角色")
    @RepeatSubmit
    @PostMapping("/{id}/admin/assignRoles")
    public void assignRoles(@ApiParam("主键") @PathVariable long id, @ApiParam("分配角色vo") @RequestBody AssignRolesVo vo) {
        umsAdminService.assignRoles(id, vo.getRoles());
    }
}
