package com.luke.xiaotiaowa.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ObjectUtil;
import com.luke.xiaotiaowa.events.XtwEvents;
import com.luke.xiaotiaowa.modules.ums.entity.UmsRoleEntity;
import com.luke.xiaotiaowa.modules.ums.service.UmsAdminService;
import com.luke.xiaotiaowa.modules.ums.service.UmsPermCategoryService;
import com.luke.xiaotiaowa.modules.ums.service.UmsRoleService;
import com.luke.xiaotiaowa.utils.JwtTokenUtil;
import com.luke.xiaotiaowa.vo.ums.UmsAdminLoginParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * token相关接口
 *
 * @author xiaotiaowa
 * @create 2023/3/24
 */
@Api(tags = "登录授权")
@RestController
@RequestMapping("auth")
public class AuthController {

    @Resource
    private UmsAdminService adminService;
    @Resource
    private UmsRoleService roleService;
    @Resource
    private UmsPermCategoryService permCategoryService;
    @Resource
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @ApiOperation(value = "登录")
    @PostMapping
    public Map<String, String> login(@Validated @RequestBody UmsAdminLoginParam umsAdminLoginParam) {
        var token = adminService.login(umsAdminLoginParam.getUsername(), umsAdminLoginParam.getPassword());
        Map<String, String> tokenMap = new HashMap<>(2);
        tokenMap.put("token", token);
        tokenMap.put("tokenHead", tokenHead);
        return tokenMap;
    }

    @ApiOperation(value = "刷新token")
    @GetMapping(value = "/refreshToken")
    @ResponseBody
    public Map<String, String> refreshToken(HttpServletRequest request) {
        var token = request.getHeader(tokenHeader);
        var refreshToken = adminService.refreshToken(token);
        XtwEvents.BAD_REQUEST.get().message("token已经过期").ifTrueThrow(CharSequenceUtil.isEmpty(refreshToken));
        Map<String, String> tokenMap = new HashMap<>(2);
        tokenMap.put("token", refreshToken);
        tokenMap.put("tokenHead", tokenHead);
        return tokenMap;
    }

    @ApiOperation(value = "获取当前登录用户信息")
    @GetMapping(value = "/info")
    @ResponseBody
    public Map<String, Object> getAdminInfo(HttpServletRequest request) {
        var token = request.getHeader(tokenHeader);
        XtwEvents.UNAUTHORIZED.get().message("Unauthorized").ifTrueThrow(ObjectUtil.isNull(token));
        var username = jwtTokenUtil.getUserNameFromToken(request.getHeader(this.tokenHeader).replace("Bearer ", ""));
        var umsAdmin = adminService.selectUmsAdminByUsername(username);
        Map<String, Object> data = new HashMap<>(3);
        data.put("username", umsAdmin.getUsername());
        data.put("admin", umsAdmin);
        data.put("perms", permCategoryService.getPermCategoryByAdminId(umsAdmin.getId()));
        data.put("icon", umsAdmin.getIcon());
        var roleList = roleService.getRolesByAdminId(umsAdmin.getId());
        if (CollUtil.isNotEmpty(roleList)) {
            var roles = roleList.stream().map(UmsRoleEntity::getName).collect(Collectors.toList());
            data.put("roles", roles);
        }
        return data;
    }

    @ApiOperation(value = "登出功能")
    @DeleteMapping(value = "/logout")
    @ResponseBody
    public String logout() {
        return "success";
    }
}
