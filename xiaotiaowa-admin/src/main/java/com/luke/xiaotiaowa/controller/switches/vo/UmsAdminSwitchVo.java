package com.luke.xiaotiaowa.controller.switches.vo;

import com.luke.xiaotiaowa.modules.ums.service.UmsAdminService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * 后台用户状态开关
 * 
 * @author xiaotiaowa
 * @create 2023/3/28
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UmsAdminSwitchVo implements ISwitchVo{
    
    /**
     * 后台用户ID
     */
    @NotEmpty
    private long id;
    
    private UmsAdminService adminService;
    
    @Override
    public void activate() {
        adminService.activateUmsAdmin(id);
    }

    @Override
    public void suspended() {
        adminService.suspendedUmsAdmin(id);
    }
}
