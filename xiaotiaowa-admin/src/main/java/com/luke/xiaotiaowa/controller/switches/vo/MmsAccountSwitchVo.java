package com.luke.xiaotiaowa.controller.switches.vo;

import com.luke.xiaotiaowa.modules.mms.service.MmsAccountService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * 用户状态开关
 * 
 * @author xiaotiaowa
 * @create 2023/4/3
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MmsAccountSwitchVo implements ISwitchVo{

    /**
     * 后台用户ID
     */
    @NotEmpty
    private long id;

    private MmsAccountService accountService;

    @Override
    public void activate() {
        accountService.activateAccount(id);
    }

    @Override
    public void suspended() {
        accountService.suspendedAccount(id);
    }
}
