package com.luke.xiaotiaowa.controller.switches.vo;

/**
 * 启用|停用开关
 * 
 * @author xiaotiaowa
 * @create 2023/3/28
 */
public interface ISwitchVo {

    /**
     * 开启
     */
    void activate();

    /**
     * 关闭
     */
    void suspended();
}
