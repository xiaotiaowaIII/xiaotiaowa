package com.luke.xiaotiaowa.controller;

import com.luke.xiaotiaowa.aspect.annotation.ExclusionLog;
import com.luke.xiaotiaowa.modules.system.entity.AdminAccessLog;
import com.luke.xiaotiaowa.modules.system.service.AdminAccessLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 系统管理相关api
 * 
 * @author xiaotiaowa
 * @create 2023/3/30
 */
@Api(tags = "系统管理")
@RestController
@RequestMapping("system")
public class SysAccessLogController {
    
    @Resource
    private AdminAccessLogService accessLogService;

    @ExclusionLog
    @ApiOperation("分页获取后台日志列表")
    @GetMapping("/admin/accessLog")
    public Page<AdminAccessLog> accseeLogPage(@ApiParam("用户名") @RequestParam(value = "username", required = false) String username,
                                             @ApiParam("每页数量") @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                             @ApiParam("页码") @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        return accessLogService.getByPage(username, pageNum, pageSize);
    }
}
