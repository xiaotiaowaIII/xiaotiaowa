package com.luke.xiaotiaowa.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luke.xiaotiaowa.context.MainThreadLocal;
import com.luke.xiaotiaowa.controller.switches.MmsAccountSwitches;
import com.luke.xiaotiaowa.controller.switches.vo.MmsAccountSwitchVo;
import com.luke.xiaotiaowa.modules.mms.entity.MmsAccountEntity;
import com.luke.xiaotiaowa.modules.mms.service.MmsAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户信息api
 * 
 * @author xiaotiaowa
 * @create 2023/4/1
 */
@Api(tags = "用户模块")
@RestController
@RequestMapping("mms")
public class MmsAccountController {
    
    @Resource
    private MmsAccountService accountService;

    @ApiOperation("分页获取用户列表")
    @GetMapping("account")
    public List<MmsAccountEntity> umsAdminPage(@ApiParam("用户名或昵称") @RequestParam(value = "username", required = false) String username,
                                                @ApiParam("账号") @RequestParam(value = "accountNumber", required = false) String accountNumber,
                                               @ApiParam("每页数量") @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                               @ApiParam("页码") @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        var page = new Page<MmsAccountEntity>(pageNum, pageSize, true);
        MainThreadLocal.setPage(page);
        return accountService.selectListByPage(page, username, accountNumber);
    }

    @ApiOperation("启用或禁用用户")
    @PutMapping("/{id}/account/{opt:activate|suspended}")
    public void activateOrSuspendedAccount(@ApiParam("主键") @PathVariable long id, @ApiParam("activate or suspended") @PathVariable String opt) {
        final var strategy = MmsAccountSwitches.strategy(opt);
        var vo = MmsAccountSwitchVo.builder()
                .id(id)
                .accountService(accountService)
                .build();
        assert strategy != null;
        strategy.accept(vo);
    }
}
