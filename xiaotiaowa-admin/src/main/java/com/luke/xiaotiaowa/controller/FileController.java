package com.luke.xiaotiaowa.controller;

import com.luke.xiaotiaowa.modules.oss.OssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 文件操作api
 * 
 * @author xiaotiaowa
 * @create 2023/4/4
 */
@Api(tags = "文件操作")
@RestController
@RequestMapping("file")
public class FileController {
    
    @Resource
    private OssService ossService;

    @ApiOperation(value = "上传文件")
    @PostMapping("upload")
    public String upload(@ApiParam(name = "file", value = "文件", required = true) @RequestParam("file") MultipartFile file) {
        return ossService.upload(file);
    }
}
