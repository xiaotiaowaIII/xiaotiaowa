package com.luke.xiaotiaowa.controller.switches;

import cn.hutool.core.text.CharSequenceUtil;
import com.luke.xiaotiaowa.controller.switches.vo.ISwitchVo;

import java.util.function.Consumer;

/**
 * 启用|停用开关
 * 
 * @author xiaotiaowa
 * @create 2023/4/3
 */
public enum MmsAccountSwitches implements Consumer<ISwitchVo> {

    /**
     * 启用
     */
    ACTIVATE() {
        @Override
        public void accept(ISwitchVo t) {
            t.activate();
        }

    },

    /**
     * 停止
     */
    SUSPENDED() {
        @Override
        public void accept(ISwitchVo t) {
            t.suspended();
        }
    };

    /**
     * 选择策略
     * @param name ^activate|suspended$
     * @return UmsAdminSwitches
     */
    public static MmsAccountSwitches strategy(String name) {
        for (MmsAccountSwitches el : values()) {
            if (CharSequenceUtil.equalsIgnoreCase(name, el.name())) {
                return el;
            }
        }
        return null;
    }
}
