package com.luke.xiaotiaowa.controller;

import com.luke.xiaotiaowa.component.DynamicSecurityMetadataSource;
import com.luke.xiaotiaowa.modules.ums.entity.UmsPermEntity;
import com.luke.xiaotiaowa.modules.ums.service.UmsPermService;
import com.luke.xiaotiaowa.valid.IGroups;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.groups.Default;

/**
 * <p>
 * 权限表 前端控制器
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Api(tags = "后台权限管理")
@RestController
@RequestMapping("/ums")
public class UmsPermController {
    
    @Resource
    private UmsPermService umsPermService;
    @Resource
    private DynamicSecurityMetadataSource dynamicSecurityMetadataSource;
    
    @ApiOperation("新增权限")
    @PostMapping("perm")
    public UmsPermEntity addUmsAdmin(@Validated({Default.class, IGroups.ADD.class}) @RequestBody UmsPermEntity perm) {
        dynamicSecurityMetadataSource.clearDataSource();
        return umsPermService.addPerm(perm);
    }

    @ApiOperation("编辑权限")
    @PutMapping("/{id}/perm")
    public UmsPermEntity modPerm(@ApiParam("主键") @PathVariable long id ,
                                                 @Validated({Default.class, IGroups.MOD.class}) @RequestBody UmsPermEntity perm) {
        dynamicSecurityMetadataSource.clearDataSource();
        return umsPermService.modPerm(id, perm);
    }

    @ApiOperation("删除权限")
    @DeleteMapping("/{id}/perm")
    public void delPerm(@ApiParam("主键") @PathVariable long id) {
        dynamicSecurityMetadataSource.clearDataSource();
        umsPermService.delPerm(id);
    }
}
