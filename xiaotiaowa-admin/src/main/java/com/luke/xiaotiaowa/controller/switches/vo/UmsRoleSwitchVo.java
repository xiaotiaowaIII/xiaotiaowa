package com.luke.xiaotiaowa.controller.switches.vo;

import com.luke.xiaotiaowa.modules.ums.service.UmsRoleService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @author xiaotiaowa
 * @create 2023/3/29
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UmsRoleSwitchVo implements ISwitchVo{

    /**
     * 角色ID
     */
    @NotEmpty
    private long id;

    private UmsRoleService roleService;

    @Override
    public void activate() {
        roleService.activateUmsRole(id);
    }

    @Override
    public void suspended() {
        roleService.suspendedUmsRole(id);
    }
}
