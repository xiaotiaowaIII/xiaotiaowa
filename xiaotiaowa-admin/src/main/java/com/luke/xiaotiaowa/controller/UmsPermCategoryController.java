package com.luke.xiaotiaowa.controller;

import com.luke.xiaotiaowa.modules.ums.entity.UmsPermCategoryEntity;
import com.luke.xiaotiaowa.modules.ums.service.UmsPermCategoryService;
import com.luke.xiaotiaowa.valid.IGroups;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.groups.Default;
import java.util.List;

/**
 * <p>
 * 模块分类表 前端控制器
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Api(tags = "后台权限分类管理")
@RestController
@RequestMapping("/ums")
public class UmsPermCategoryController {
    
    @Resource
    private UmsPermCategoryService permCategoryService;

    @ApiOperation("查询角色拥有的权限分类")
    @GetMapping("/perms_category/{roleId}/role")
    public List<UmsPermCategoryEntity> getPermCategorysByRoleId(@ApiParam("角色id") @PathVariable long roleId) {
        return permCategoryService.getPermCategoryByRoleId(roleId);
    }


    @ApiOperation("查询所有后台权限分类及子类")
    @GetMapping(value = "/perm_category/listAll")
    public List<UmsPermCategoryEntity> listAll() {
        return permCategoryService.listAll();
    }

    @ApiOperation("新增权限分类")
    @PostMapping("perm_category")
    public UmsPermCategoryEntity addUmsAdmin(@Validated({Default.class, IGroups.ADD.class}) @RequestBody UmsPermCategoryEntity permCategory) {
        return permCategoryService.addPermCategory(permCategory);
    }

    @ApiOperation("编辑权限分类")
    @PutMapping("/{id}/perm_category")
    public UmsPermCategoryEntity modPermCategory(@ApiParam("主键") @PathVariable long id ,
                                                 @Validated({Default.class, IGroups.MOD.class}) @RequestBody UmsPermCategoryEntity permCategory) {
        return permCategoryService.modPermCategory(id, permCategory);
    }

    @ApiOperation("删除权限分类")
    @DeleteMapping("/{id}/perm_category")
    public void delPermCategory(@ApiParam("主键") @PathVariable long id) {
        permCategoryService.delPermCategory(id);
    }
}
