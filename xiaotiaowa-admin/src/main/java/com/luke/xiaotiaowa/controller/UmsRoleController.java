package com.luke.xiaotiaowa.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luke.xiaotiaowa.context.MainThreadLocal;
import com.luke.xiaotiaowa.controller.switches.UmsRoleSwitches;
import com.luke.xiaotiaowa.controller.switches.vo.UmsRoleSwitchVo;
import com.luke.xiaotiaowa.modules.ums.entity.UmsRoleEntity;
import com.luke.xiaotiaowa.modules.ums.service.UmsRoleService;
import com.luke.xiaotiaowa.valid.IGroups;
import com.luke.xiaotiaowa.vo.ums.AssignPermsVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.groups.Default;
import java.util.List;

/**
 * <p>
 * 角色角色表 前端控制器
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Api(tags = "角色角色管理")
@RestController
@RequestMapping("/ums")
public class UmsRoleController {
    
    @Resource
    private UmsRoleService roleService;

    @ApiOperation("查询所有角色")
    @GetMapping("/roles")
    public List<UmsRoleEntity> roles() {
        return roleService.list();
    }
    
    @ApiOperation("查询用户拥有的角色")
    @GetMapping("/roles/{adminId}/admin")
    public List<UmsRoleEntity> getRolesByAdminId(@ApiParam("用户id") @PathVariable long adminId) {
        return roleService.getRolesByAdminId(adminId);
    }

    @ApiOperation("根据角色或描述分页获取角色列表")
    @GetMapping("role")
    public List<UmsRoleEntity> umsRolePage(@ApiParam("搜索条件") @RequestParam(value = "keyword", required = false) String keyword,
                                             @ApiParam("每页数量") @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                             @ApiParam("页码") @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        var page = new Page<UmsRoleEntity>(pageNum, pageSize, true);
        MainThreadLocal.setPage(page);
        return roleService.selectUmsRole(page, keyword);
    }

    @ApiOperation("通过主键查询单条数据")
    @GetMapping("/{id}/role")
    public UmsRoleEntity umsRole(@ApiParam("主键") @PathVariable long id) {
        return roleService.selectUmsRole(id);
    }

    @ApiOperation("新增角色")
    @PostMapping("role")
    public UmsRoleEntity addUmsRole(@Validated({Default.class, IGroups.ADD.class}) @RequestBody UmsRoleEntity umsRole) {
        return roleService.addUmsRole(umsRole);
    }

    @ApiOperation("编辑角色")
    @PutMapping("/{id}/role")
    public UmsRoleEntity modUmsRole(@ApiParam("主键") @PathVariable long id,
                                    @Validated({Default.class, IGroups.MOD.class}) @RequestBody UmsRoleEntity umsRole) {
        return roleService.modUmsRole(id, umsRole);
    }

    @ApiOperation("删除角色")
    @DeleteMapping("/{id}/role")
    public void delUmsRole(@ApiParam("主键") @PathVariable long id) {
        roleService.delUmsRole(id);
    }

    @ApiOperation("启用或禁用角色")
    @PutMapping("/{id}/role/{opt:activate|suspended}")
    public void activateOrSuspendedAdmin(@ApiParam("主键") @PathVariable long id, @ApiParam("activate or suspended") @PathVariable String opt) {
        final var strategy = UmsRoleSwitches.strategy(opt);
        var vo = UmsRoleSwitchVo.builder().build();
        vo.setId(id);
        vo.setRoleService(roleService);
        assert strategy != null;
        strategy.accept(vo);
    }

    @ApiOperation("分配权限")
    @PostMapping("/{id}/role/assignPerms")
    public void assignPerm(@ApiParam("主键") @PathVariable long id, @ApiParam("分配权限vo") @RequestBody AssignPermsVo vo) {
        roleService.assignPerm(id, vo.getPerms());
    }
}
