package com.luke.xiaotiaowa;

import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * 应用启动入口
 * 
 * @author xiaotiaowa
 * @create 2023/3/22
 */
@Slf4j
@EnableOpenApi
@SpringBootApplication
public class XtwAdminApplication {

    public static void main(String[] args) {
        var run = SpringApplication.run(XtwAdminApplication.class, args);
        XtwAdminApplication.afterStartup(run);
    }

    /**
     * 启动后执行
     *
     * @param run ConfigurableApplicationContext
     */
    private static void afterStartup(ConfigurableApplicationContext run) {
        log.info(">>> run flyway");
        // 执行 flyway sql迭代
        var flyway = run.getBean(Flyway.class);
        flyway.baseline();
        flyway.migrate();
        log.info(">>> run flyway end");
    }
}
