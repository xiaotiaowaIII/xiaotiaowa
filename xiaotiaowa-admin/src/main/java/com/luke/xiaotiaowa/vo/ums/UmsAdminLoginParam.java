package com.luke.xiaotiaowa.vo.ums;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * 用户登录对象
 *
 * @author xiaotiaowa
 * @create 2023/3/24
 */
@Getter
@Setter
@EqualsAndHashCode
@ApiModel(value = "UmsAdminLoginParam", description = "登录对象")
public class UmsAdminLoginParam {

    @NotEmpty
    @ApiModelProperty(value = "用户名", required = true)
    private String username;
    @NotEmpty
    @ApiModelProperty(value = "密码", required = true)
    private String password;
}
