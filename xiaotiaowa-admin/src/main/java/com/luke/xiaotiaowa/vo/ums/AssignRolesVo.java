package com.luke.xiaotiaowa.vo.ums;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xiaotiaowa
 * @create 2023/3/29
 */
@Getter
@Setter
public class AssignRolesVo {
    
    @ApiModelProperty("角色集")
    private List<Long> roles;
}
