package com.luke.xiaotiaowa.vo.ums;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author xiaotiaowa
 * @create 2023/3/29
 */
@Getter
@Setter
public class AssignPermsVo {

    @ApiModelProperty("权限分类id集")
    private List<Long> perms;
}
