CREATE TABLE `ums_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `admin_count` int(11) NULL DEFAULT NULL COMMENT '后台用户数量',
  `status` int(1) NULL DEFAULT 1 COMMENT '启用状态：0->禁用；1->启用',
  `sort` int(11) NULL DEFAULT 0,
  `version` bigint NULL DEFAULT 0 COMMENT '乐观锁',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `lst_mod_time` datetime NULL COMMENT '最后修改人',
  `lst_mod_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台用户角色表' ROW_FORMAT = Dynamic;

INSERT INTO `ums_role` VALUES (1, '帖子管理员', '只能查看及操作帖子', 0, 1, 0, 0, '2023-03-24 19:00:00', 'admin', null, null);
INSERT INTO `ums_role` VALUES (2, '运营管理员', '只能查看及操作运营', 0, 1, 0, 0, '2023-03-24 19:00:00', 'admin', null, null);
INSERT INTO `ums_role` VALUES (3, '用户管理员', '只能查看及操作用户', 0, 1, 0, 0, '2023-03-24 19:00:00', 'admin', null, null);
INSERT INTO `ums_role` VALUES (4, '权限管理员', '只能查看及操作权限', 0, 1, 0, 0, '2023-03-24 19:00:00', 'admin', null, null);
INSERT INTO `ums_role` VALUES (5, '超级管理员', '拥有所有查看和操作功能', 0, 1, 0, 0, '2023-03-24 19:00:00', 'admin', null, null);

CREATE TABLE `ums_perm`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限URL',
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `category_id` bigint(20) NULL DEFAULT NULL COMMENT '模块分类ID',
  `version` bigint NULL DEFAULT 0 COMMENT '乐观锁',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `lst_mod_time` datetime NULL COMMENT '最后修改人',
  `lst_mod_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

INSERT INTO `ums_perm` VALUES (1, '后台用户管理', '/ums/admin/**', '', 4, 0, '2023-03-24 19:00:00', 'admin', null, null);
INSERT INTO `ums_perm` VALUES (2, '后台用户角色管理', '/ums/role/**', '', 4, 0, '2023-03-24 19:00:00', 'admin', null, null);
INSERT INTO `ums_perm` VALUES (3, '后台权限管理', '/ums/perm/**', '', 4, 0, '2023-03-24 19:00:00', 'admin', null, null);
INSERT INTO `ums_perm` VALUES (4, '后台权限分类管理', '/ums/perm_category/**', '', 4, 0, '2023-03-24 19:00:00', 'admin', null, null);

CREATE TABLE `ums_perm_category`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块名称',
  `sort` int(4) NULL DEFAULT NULL COMMENT '排序',
  `version` bigint NULL DEFAULT 0 COMMENT '乐观锁',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `lst_mod_time` datetime NULL COMMENT '最后修改人',
  `lst_mod_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '模块分类表' ROW_FORMAT = Dynamic;

INSERT INTO `ums_perm_category` VALUES (1, '帖子模块', 0, 0, '2023-03-24 19:00:00', 'admin', null, null);
INSERT INTO `ums_perm_category` VALUES (2, '用户模块', 0, 0, '2023-03-24 19:00:00', 'admin', null, null);
INSERT INTO `ums_perm_category` VALUES (3, '运营模块', 0, 0, '2023-03-24 19:00:00', 'admin', null, null);
INSERT INTO `ums_perm_category` VALUES (4, '权限模块', 0, 0, '2023-03-24 19:00:00', 'admin', null, null);

CREATE TABLE `ums_admin_role_relation`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `role_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台用户和角色关系表' ROW_FORMAT = Dynamic;

INSERT INTO `ums_admin_role_relation` VALUES (1, 1, 5);

CREATE TABLE `ums_role_perm_relation`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL,
  `perm_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台用户角色和权限关系表' ROW_FORMAT = Dynamic;

INSERT INTO `ums_role_perm_relation` VALUES (1, 5, 1);
INSERT INTO `ums_role_perm_relation` VALUES (2, 5, 2);
INSERT INTO `ums_role_perm_relation` VALUES (3, 5, 3);
INSERT INTO `ums_role_perm_relation` VALUES (4, 5, 4);

