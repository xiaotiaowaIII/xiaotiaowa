package com.luke.xiaotiaowa.config;

import com.luke.xiaotiaowa.events.XtwEvents;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.Configuration;
import org.flywaydb.core.api.output.CleanResult;

/**
 * 包一层，去除clean
 * 
 * @author xiaotiaowa
 * @create 2023/3/22
 */
public class NonCleanFlyway extends Flyway {

    public NonCleanFlyway(Configuration configuration) {
        super(configuration);
    }

    @Override
    public CleanResult clean() {
        throw XtwEvents.OTHERS.get().message("不允许执行");
    }
}
