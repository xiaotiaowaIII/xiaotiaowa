package com.luke.xiaotiaowa.config;

import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

/**
 * flyway相关配置
 * 
 * @author xiaotiaowa
 * @create 2023/3/22
 */
@Slf4j
@Configuration
public class FlywayConfig {

    public static final String MIGRATION = "db/migration";
    public static final String SAMPLEDATA = "db/sampledata";
    public static final String TEST = "db/test";

    /**
     * 部署环境下，flyway 配置
     * @param dataSource 数据源
     * @return ClassicConfiguration
     */
    @Profile({ "dev" })
    @Bean
    public ClassicConfiguration flywayConfDev(DataSource dataSource) {
        log.info(">>> flyway profiles is not test");
        var cfg = new ClassicConfiguration();
        cfg.setDataSource(dataSource);
        cfg.setLocationsAsStrings(MIGRATION, SAMPLEDATA);
        return cfg;
    }

    @Profile({ "prod" })
    @Bean
    public ClassicConfiguration flywayConfProd(DataSource dataSource) {
        log.info(">>> flyway profiles is not test");
        var cfg = new ClassicConfiguration();
        cfg.setDataSource(dataSource);
        cfg.setLocationsAsStrings(MIGRATION);
        return cfg;
    }
  

    /**
     * 本地测试下，flyway 配置
     * @param dataSource 数据源
     * @return ClassicConfiguration
     */
    @Profile({ "test" })
    @Bean
    public ClassicConfiguration flywayConfTest(DataSource dataSource) {
        log.info(">>> flyway profiles is test");
        var cfg = new ClassicConfiguration();
        cfg.setDataSource(dataSource);
        cfg.setLocationsAsStrings(MIGRATION, TEST);
        return cfg;
    }

    /**
     * flyway bean，这里为了安全，会删除 clean() 方法
     * @param cfg ClassicConfiguration
     * @return Flyway
     */
    @Bean
    public Flyway flyway(ClassicConfiguration cfg) {
        return new NonCleanFlyway(cfg);
    }
}
