package com.luke.xiaotiaowa.bo;

import com.luke.xiaotiaowa.modules.ums.entity.UmsAdminEntity;
import com.luke.xiaotiaowa.modules.ums.entity.UmsPermEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xiaotiaowa
 * @create 2023/3/24
 */
public class AdminUserDetails implements UserDetails, Serializable {

    private static final long serialVersionUID = 6231069819211839636L;
    
    /**
     * 后台用户
     */
    private final UmsAdminEntity umsAdmin;

    /**
     * 用戶所拥有的权限
     */
    private final List<UmsPermEntity> perms;

    public AdminUserDetails(UmsAdminEntity umsAdmin, List<UmsPermEntity> perms) {
        this.umsAdmin = umsAdmin;
        this.perms = perms;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return perms.stream()
                .map(role -> new SimpleGrantedAuthority(role.getId() + ":" + role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return umsAdmin.getPassword();
    }

    @Override
    public String getUsername() {
        return umsAdmin.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return umsAdmin.isStatus();
    }
}
