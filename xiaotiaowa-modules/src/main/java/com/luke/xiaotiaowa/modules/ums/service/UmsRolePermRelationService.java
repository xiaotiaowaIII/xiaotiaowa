package com.luke.xiaotiaowa.modules.ums.service;

import com.luke.xiaotiaowa.modules.ums.entity.UmsRolePermRelationEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台用户角色和权限关系表 服务类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
public interface UmsRolePermRelationService extends IService<UmsRolePermRelationEntity> {

}
