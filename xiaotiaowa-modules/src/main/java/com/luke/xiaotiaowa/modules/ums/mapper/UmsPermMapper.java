package com.luke.xiaotiaowa.modules.ums.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luke.xiaotiaowa.modules.ums.entity.UmsPermEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Mapper
public interface UmsPermMapper extends BaseMapper<UmsPermEntity> {

    /**
     * 查询用户所拥有的权限
     * @param adminId 用户id
     * @return list
     */
    List<UmsPermEntity> selectListByAdminId(long adminId);

    /**
     * 查询所有权限
     * @return list
     */
    List<UmsPermEntity> selectAll();
}
