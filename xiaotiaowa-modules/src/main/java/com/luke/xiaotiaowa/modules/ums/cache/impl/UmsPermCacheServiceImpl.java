package com.luke.xiaotiaowa.modules.ums.cache.impl;

import com.luke.xiaotiaowa.modules.redis.RedisService;
import com.luke.xiaotiaowa.modules.ums.cache.UmsPermCacheService;
import com.luke.xiaotiaowa.modules.ums.entity.UmsAdminEntity;
import com.luke.xiaotiaowa.modules.ums.entity.UmsPermEntity;
import com.luke.xiaotiaowa.modules.ums.mapper.UmsAdminMapper;
import com.luke.xiaotiaowa.properties.XtwProperties;
import jodd.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 权限缓存实现类
 * 
 * @author xiaotiaowa
 * @create 2023/3/31
 */
@Slf4j
@Service
public class UmsPermCacheServiceImpl implements UmsPermCacheService {
    
    @Resource
    private RedisService redisService;
    @Resource
    private XtwProperties prop;
    @Resource
    private UmsAdminMapper adminMapper;

    private String buildKey() {
        return prop.getRedis().getDatabase() + ":" + prop.getRedis().getKey().get("perms") + ":%d";
    }

    @Override
    public List<UmsPermEntity> getPerms(long adminId) {
        return (List<UmsPermEntity>) redisService.listGetAll(String.format(buildKey(), adminId));
    }

    @Override
    public void setPerms(long adminId, List<UmsPermEntity> perms) {
        redisService.listSet(String.format(buildKey(), adminId), 28800, perms);
    }

    @Override
    public void delPerm(long adminId) {
        redisService.listDelAll(String.format(buildKey(), adminId));
    }

    @Override
    public void delPermAsync(long adminId, long time) {
        async("thread-delAdminPermsCacheKey-%d", time, () -> {
            redisService.listDelAll(String.format(buildKey(), adminId));
        });
    }

    @Override
    public void delByPermId(long permId) {
        var admins = adminMapper.selectListByPermId(permId);
        for (UmsAdminEntity admin : admins) {
            delPerm(admin.getId());
        }
    }

    @Override
    public void delByRoleId(long roleId) {
        log.info("del cache : " + LocalDateTime.now());
        var admins = adminMapper.selectListByRoleId(roleId);
        for (UmsAdminEntity admin : admins) {
            delPerm(admin.getId());
        }
    }

    @Override
    public void delByPermIdAsync(long permId, long time) {
        async("thread-delAdminPermsByPermIdCacheKey-%d", time, () -> {
            delByPermId(permId);
        });
    }

    @Override
    public void delByRoleIdAsync(long roleId, long time) {
        async("thread-delAdminPermsByRoleIdCacheKey-%d", time, () -> {
            delByRoleId(roleId);
        });
    }

    private void async(String threadName, long time, Runnable runnable) {
        var namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat(threadName).get();
        var singleThreadPool = new ThreadPoolExecutor(1, 5,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());
        singleThreadPool.execute(() -> {
            try {
                Thread.sleep(time);
                runnable.run();
            } catch (InterruptedException e) {
                log.error("Interrupted！", e);
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
        });
        singleThreadPool.shutdown();
    }
}
