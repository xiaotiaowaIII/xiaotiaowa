package com.luke.xiaotiaowa.modules.ums.mapper;

import com.luke.xiaotiaowa.modules.ums.entity.UmsAdminRoleRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 后台用户和角色关系表 Mapper 接口
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Mapper
public interface UmsAdminRoleRelationMapper extends BaseMapper<UmsAdminRoleRelationEntity> {

    /**
     * 删除用户已经分配的角色
     * @param adminId 用户id
     */
    void deleteByAdminId(long adminId);

    /**
     * 删除角色
     * @param roleId 角色id
     */
    void deleteByRoleId(long roleId);
}
