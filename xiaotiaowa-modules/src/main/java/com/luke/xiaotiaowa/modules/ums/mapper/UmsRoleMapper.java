package com.luke.xiaotiaowa.modules.ums.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luke.xiaotiaowa.modules.ums.entity.UmsRoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 后台用户角色表 Mapper 接口
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Mapper
public interface UmsRoleMapper extends BaseMapper<UmsRoleEntity> {

    /**
     * 查询用户所拥有的角色
     * @param adminId 用户id
     * @return 角色集
     */
    List<UmsRoleEntity> selectRolesByAdminId(long adminId);

    /**
     * 分页查询
     * @param page 分页信息
     * @param keyword 查询条件
     * @return 角色集
     */
    List<UmsRoleEntity> selectListByParams(IPage<UmsRoleEntity> page, String keyword);

    /**
     * 查询角色
     * @param name 角色名称
     * @return UmsRoleEntity
     */
    UmsRoleEntity selectByUsername(String name);
}
