package com.luke.xiaotiaowa.modules.ums.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luke.xiaotiaowa.modules.ums.entity.UmsAdminEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 后台管理员表 Mapper 接口
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-22
 */
@Mapper
public interface UmsAdminMapper extends BaseMapper<UmsAdminEntity> {

    /**
     * 后台用户列表
     * @param page 分页
     * @param keyword 用户名或昵称
     * @return list
     */
    List<UmsAdminEntity> selectListByParams(IPage<UmsAdminEntity> page, String keyword);

    /**
     * 通过用户名查询单条后台用户数据
     * @param username 用户名
     * @return UmsAdminEntity
     */
    UmsAdminEntity selectByUsername(String username);

    /**
     * 查询后台用户
     * @param roleId 角色id
     * @return list
     */
    List<UmsAdminEntity> selectListByRoleId(long roleId);

    /**
     * 查询后台用户
     * @param permId 权限id
     * @return list
     */
    List<UmsAdminEntity> selectListByPermId(long permId);
}
