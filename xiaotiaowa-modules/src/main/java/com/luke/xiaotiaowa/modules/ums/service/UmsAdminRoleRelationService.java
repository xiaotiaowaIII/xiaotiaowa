package com.luke.xiaotiaowa.modules.ums.service;

import com.luke.xiaotiaowa.modules.ums.entity.UmsAdminRoleRelationEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台用户和角色关系表 服务类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
public interface UmsAdminRoleRelationService extends IService<UmsAdminRoleRelationEntity> {

}
