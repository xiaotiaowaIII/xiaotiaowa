package com.luke.xiaotiaowa.modules.system.repository;

import com.luke.xiaotiaowa.modules.system.entity.AdminAccessLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * 后台操作日志 mongoDB操作
 * 
 * @author xiaotiaowa
 * @create 2023/3/30
 */
public interface AdminAccessLogRepository extends MongoRepository<AdminAccessLog, String> {

    /**
     * 分页查询操作日志
     * @param pageable 分页信息
     * @return Page<AdminAccessLogEntity>
     */
    Page<AdminAccessLog> findAllByOrderByStartTimeDesc(Pageable pageable);

    /**
     * 分页查询操作日志
     * @param username 用户名
     * @param pageable 分页信息
     * @return Page<AdminAccessLogEntity>
     */
    Page<AdminAccessLog> findByUsernameOrderByStartTimeDesc(String username, Pageable pageable);
    
}
