package com.luke.xiaotiaowa.modules.ums.mapper;

import com.luke.xiaotiaowa.modules.ums.entity.UmsRolePermRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 后台用户角色和权限关系表 Mapper 接口
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Mapper
public interface UmsRolePermRelationMapper extends BaseMapper<UmsRolePermRelationEntity> {

    /**
     * 删除
     * @param roleId 角色id
     */
    void deleteByRoleId(long roleId);

    /**
     * 删除
     * @param permId 权限id
     */
    void deleteByPermId(long permId);
}
