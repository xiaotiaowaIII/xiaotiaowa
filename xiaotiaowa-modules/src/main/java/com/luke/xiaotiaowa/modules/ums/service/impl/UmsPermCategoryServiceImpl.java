package com.luke.xiaotiaowa.modules.ums.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.luke.xiaotiaowa.events.XtwEvents;
import com.luke.xiaotiaowa.modules.ums.entity.UmsPermCategoryEntity;
import com.luke.xiaotiaowa.modules.ums.mapper.UmsPermCategoryMapper;
import com.luke.xiaotiaowa.modules.ums.mapper.UmsPermMapper;
import com.luke.xiaotiaowa.modules.ums.mapper.UmsRolePermRelationMapper;
import com.luke.xiaotiaowa.modules.ums.service.UmsPermCategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 模块分类表 服务实现类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Service
public class UmsPermCategoryServiceImpl implements UmsPermCategoryService {
    
    @Resource
    private UmsPermCategoryMapper permCategoryMapper;
    @Resource
    private UmsPermMapper permMapper;
    @Resource
    private UmsRolePermRelationMapper rolePermRelationMapper;

    @Override
    public List<UmsPermCategoryEntity> getPermCategoryByAdminId(long adminId) {
        return permCategoryMapper.selectByAdminId(adminId);
    }

    @Override
    public List<UmsPermCategoryEntity> getPermCategoryByRoleId(long roleId) {
        return permCategoryMapper.selectByRoleId(roleId);
    }

    @Override
    public List<UmsPermCategoryEntity> listAll() {
        var category = permCategoryMapper.selectAll();
        var perms = permMapper.selectAll();
        category.forEach(c -> {
            var perm = perms.stream()
                    .filter(p -> c.getId().equals(p.getCategoryId()))
                    .collect(Collectors.toList());
            c.setChildren(perm);
        });
        return category;
    }

    @Override
    public UmsPermCategoryEntity selectPermCategory(long id) {
        var permCategory =  permCategoryMapper.selectById(id);
        XtwEvents.BAD_REQUEST.get().message("权限分类不存在")
                .ifTrueThrow(ObjectUtil.isNull(permCategory));
        return permCategory;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UmsPermCategoryEntity addPermCategory(UmsPermCategoryEntity permCategory) {
        permCategoryMapper.insert(permCategory);
        return permCategory;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UmsPermCategoryEntity modPermCategory(long id, UmsPermCategoryEntity permCategory) {
        permCategory.setId(id);
        var oldPermCategory = selectPermCategory(id);
        permCategory.setVersion(oldPermCategory.getVersion());
        permCategoryMapper.updateById(permCategory);
        return permCategory;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delPermCategory(long id) {
        var permCategory = selectPermCategory(id);
        rolePermRelationMapper.deleteByPermId(permCategory.getId());
        permCategoryMapper.deleteById(permCategory);
    }
}
