package com.luke.xiaotiaowa.modules.ums.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.luke.xiaotiaowa.common.BaseEntity;
import com.luke.xiaotiaowa.valid.IGroups;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Getter
@Setter
@TableName("ums_perm")
@Alias("ums_perm")
@ApiModel(value = "UmsPermEntity对象", description = "权限表")
public class UmsPermEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Null(groups = IGroups.ADD.class)
    @NotNull(groups = IGroups.MOD.class)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @NotBlank
    @ApiModelProperty("权限名称")
    private String name;

    @NotBlank
    @ApiModelProperty("权限URL")
    private String url;

    @ApiModelProperty("描述")
    private String description;

    @NotNull
    @ApiModelProperty("模块分类ID")
    private Long categoryId;

    @ApiModelProperty("乐观锁")
    @Version
    private Long version;


}
