package com.luke.xiaotiaowa.modules.redis;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis操作接口
 *
 * @author xiaotiaowa
 * @create 2023/3/29
 */
public interface RedisService {

    /**
     * 保存（会过期）
     *
     * @param key   key
     * @param value 值
     * @param time  有效时长
     */
    void set(String key, Object value, long time);

    /**
     * 保存（会过期）
     *
     * @param key   key
     * @param value 值
     * @param time  有效时长
     * @param timeUnit 单位
     */
    void set(String key, Object value, long time, TimeUnit timeUnit);

    /**
     * 保存（不会过期）
     *
     * @param key   key
     * @param value 值
     */
    void set(String key, Object value);

    /**
     * 获取
     *
     * @param key   key
     * @param clazz 转换对象
     * @return Object
     */
    <T> T get(String key, Class<T> clazz);

    /**
     * 删除
     *
     * @param key key
     * @return Boolean
     */
    String del(String key);

    /**
     * 设置过期时间
     *
     * @param key  key
     * @param time 过期时长
     * @return Boolean
     */
    String expire(String key, long time);

    /**
     * 向Hash结构中放入一个属性
     *
     * @param key   key
     * @param hKey  hash key
     * @param value 值
     * @param time  过期时长
     */
    void hashSet(String key, String hKey, Object value, long time);

    /**
     * Hash中获取
     *
     * @param key   key
     * @param hKey  hash key
     * @return T
     */
    Object hashGet(String key, String hKey);

    /**
     * hash中获取所有hash key
     *
     * @param key key
     * @return set
     */
    Set<String> hashKeys(String key);

    /**
     * Hash中移除
     *
     * @param key  key
     * @param hKey hash key
     */
    void hashDel(String key, String hKey);

    /**
     * 获取list缓存
     *
     * @param key key
     * @return List<T>
     */
    Object listGetAll(String key);

    /**
     * 获取List结构的长度
     *
     * @param key key
     * @return long
     */
    Long listSize(String key);

    /**
     * 设置list缓存
     *
     * @param key     key
     * @param objects 对象
     * @param time    有效时长
     */
    void listSet(String key, long time, List<?> objects);

    /**
     * 移除list缓存
     *
     * @param key key
     */
    void listDelAll(String key);

    /**
     * 移除list缓存
     * @param keys key 多个
     */
    void listDel(List<String> keys);
}
