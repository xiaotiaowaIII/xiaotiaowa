package com.luke.xiaotiaowa.modules.ums.cache;

import com.luke.xiaotiaowa.modules.ums.entity.UmsAdminEntity;

/**
 * 后台用户缓存接口
 * 
 * @author xiaotiaowa
 * @create 2023/3/29
 */
public interface UmsAdminCacheService {
    
    /**
     * 获取后台用户
     * @param username 用户名
     * @return UmsAdminEntity
     */
    UmsAdminEntity getAdminByUsername(String username);

    /**
     * 缓存后台用户信息
     * @param admin 用户信息
     */
    void setAdmin(UmsAdminEntity admin);

    /**
     * 移除后台用户信息的缓存
     * @param username 用户名
     */
    void delAdmin(String username);

    /**
     * 移除后台用户信息的缓存
     * @param username 用户名
     * @param time 延时时间
     */
    void delAdminAsync(String username, long time);
}
