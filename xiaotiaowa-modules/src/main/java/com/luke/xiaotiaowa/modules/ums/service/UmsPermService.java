package com.luke.xiaotiaowa.modules.ums.service;

import com.luke.xiaotiaowa.modules.ums.entity.UmsPermEntity;

import java.util.List;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
public interface UmsPermService {

    /**
     * 查找所有权限
     * @return list
     */
    List<UmsPermEntity> getAll();


    /**
     * 查询权限
     * @param id id
     * @return 权限分类集
     */
    UmsPermEntity selectPerm(long id);

    /**
     * 查询用户所拥有的权限
     * @param adminId 用户id
     * @return list
     */
    List<UmsPermEntity> getPerms(long adminId);

    /**
     * 新增权限
     * @param perm entity
     * @return UmsPermEntity
     */
    UmsPermEntity addPerm(UmsPermEntity perm);


    /**
     * 编辑权限
     * @param id 主键
     * @param perm entity
     * @return UmsPermEntity
     */
    UmsPermEntity modPerm(long id, UmsPermEntity perm);

    /**
     * 删除权限
     * @param id id
     */
    void delPerm(long id);
}
