package com.luke.xiaotiaowa.modules.ums.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.luke.xiaotiaowa.events.XtwEvents;
import com.luke.xiaotiaowa.modules.ums.cache.UmsPermCacheService;
import com.luke.xiaotiaowa.modules.ums.entity.UmsPermEntity;
import com.luke.xiaotiaowa.modules.ums.mapper.UmsPermCategoryMapper;
import com.luke.xiaotiaowa.modules.ums.mapper.UmsPermMapper;
import com.luke.xiaotiaowa.modules.ums.service.UmsPermCategoryService;
import com.luke.xiaotiaowa.modules.ums.service.UmsPermService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Service
public class UmsPermServiceImpl implements UmsPermService {
    
    @Resource
    private UmsPermMapper permMapper;
    @Resource
    private UmsPermCategoryService permCategoryService;
    @Resource
    private UmsPermCacheService permCacheService;

    @Override
    public List<UmsPermEntity> getAll() {
                return permMapper.selectList(new QueryWrapper<>());
    }

    @Override
    public UmsPermEntity selectPerm(long id) {
        var perm =  permMapper.selectById(id);
        XtwEvents.BAD_REQUEST.get().message("权限不存在")
                .ifTrueThrow(ObjectUtil.isNull(perm));
        return perm;
    }

    @Override
    synchronized public List<UmsPermEntity> getPerms(long adminId) {
        var cachePerms = permCacheService.getPerms(adminId);
        return Optional.ofNullable(cachePerms)
                .filter(CollUtil::isNotEmpty)
                .orElseGet(() -> {
                    var perms = permMapper.selectListByAdminId(adminId);
                    if (CollUtil.isNotEmpty(perms)) {
                         permCacheService.setPerms(adminId, perms);
                    }
                    return perms;
                });
    }

    @Override
    public UmsPermEntity addPerm(UmsPermEntity perm) {
        var category = permCategoryService.selectPermCategory(perm.getCategoryId());
        perm.setCategoryId(category.getId());
        perm.setUrl(perm.getUrl()
                .replaceAll(" ", "")
                .replaceAll("，", ","));
        permMapper.insert(perm);
        return perm;
    }

    @Override
    public UmsPermEntity modPerm(long id, UmsPermEntity perm) {
        var oldPerm = selectPerm(id);
        permCacheService.delByPermId(id);
        var category = permCategoryService.selectPermCategory(perm.getCategoryId());
        perm.setCategoryId(category.getId());
        perm.setVersion(oldPerm.getVersion());
        perm.setUrl(perm.getUrl()
                .replaceAll(" ", "")
                .replaceAll("，", ","));
        permMapper.updateById(perm);
        permCacheService.delByPermIdAsync(id, 3000);
        return perm;
    }

    @Override
    public void delPerm(long id) {
        var perm = selectPerm(id);
        permCacheService.delByPermId(id);
        permMapper.deleteById(perm);
        permCacheService.delByPermIdAsync(id, 3000);
    }
}
