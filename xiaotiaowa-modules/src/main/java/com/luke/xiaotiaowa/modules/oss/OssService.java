package com.luke.xiaotiaowa.modules.oss;

import org.springframework.web.multipart.MultipartFile;

/**
 * oss 接口
 * 
 * @author xiaotiaowa
 * @create 2023/4/4
 */
public interface OssService {

    /**
     * 上传文件
     * @param file 文件
     * @return 访问url
     */
    String upload(MultipartFile file);
}
