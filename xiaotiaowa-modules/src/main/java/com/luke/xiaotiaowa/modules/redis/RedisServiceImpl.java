package com.luke.xiaotiaowa.modules.redis;

import com.luke.xiaotiaowa.utils.JsonUtils;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis操作实现类
 *
 * @author xiaotiaowa
 * @create 2023/3/29
 */
@Service
public class RedisServiceImpl implements RedisService {

    @Resource(name = "redisTemplate")
    private ValueOperations<String, String> operations;
    @Resource(name = "redisTemplate")
    private HashOperations<String, String, Object> hashOperations;
    @Resource(name = "redisTemplate")
    private ListOperations<String, Object> listOperations;
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    
    @Override
    public void set(String key, Object value, long time) {
        operations.set(key, JsonUtils.toJsonString(value), time, TimeUnit.SECONDS);
    }

    @Override
    public void set(String key, Object value, long time, TimeUnit timeUnit) {
        operations.set(key, JsonUtils.toJsonString(value), time, timeUnit);
    }

    @Override
    public void set(String key, Object value) {
        operations.set(key, JsonUtils.toJsonString(value));
    }

    @Override
    public <T> T get(String key, Class<T> clazz) {
        var value = operations.get(key);
        return Optional.ofNullable(value)
                .map(v -> JsonUtils.parseObject(value, clazz))
                .orElse(null);
    }

    @Override
    public String del(String key) {
        return operations.getAndDelete(key);
    }

    @Override
    public String expire(String key, long time) {
        return operations.getAndExpire(key, time, TimeUnit.SECONDS);
    }

    @Override
    public void hashSet(String key, String hKey, Object value, long time) {
        hashOperations.put(key, hKey, value);
        redisTemplate.expire(key, time, TimeUnit.SECONDS);
    }

    @Override
    public Object hashGet(String key, String hKey) {
        return hashOperations.get(key, hKey);
    }

    @Override
    public Set<String> hashKeys(String key) {
        return hashOperations.keys(key);
    }

    @Override
    public void hashDel(String key, String hKey) {
        hashOperations.delete(key, hKey);
    }

    @Override
    public Object listGetAll(String key) {
        return listOperations.range(key, 0, -1);
    }

    @Override
    public Long listSize(String key) {
        return listOperations.size(key);
    }

    @Override
    public void listSet(String key, long time, List<?> objects) {
        for (Object object : objects) {
            listOperations.rightPush(key, object);
        }
        redisTemplate.expire(key, time, TimeUnit.SECONDS);
    }

    @Override
    public void listDelAll(String key) {
        redisTemplate.delete(key);
    }

    @Override
    public void listDel(List<String> keys) {
        redisTemplate.delete(keys);
    }
}
