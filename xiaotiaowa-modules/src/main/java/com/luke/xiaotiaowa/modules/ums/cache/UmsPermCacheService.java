package com.luke.xiaotiaowa.modules.ums.cache;

import com.luke.xiaotiaowa.modules.ums.entity.UmsPermEntity;

import java.util.List;

/**
 * 权限缓存接口
 * 
 * @author xiaotiaowa
 * @create 2023/3/31
 */
public interface UmsPermCacheService {

    /**
     * 获取缓存 后台用户权限
     * @param adminId 后台用户id
     * @return list
     */
    List<UmsPermEntity> getPerms(long adminId);

    /**
     * 缓存后台用户权限
     * @param adminId 后台用户id
     * @param perms 拥有的权限
     */
    void setPerms(long adminId, List<UmsPermEntity> perms);

    /**
     * 移除缓存后台用户权限
     * @param adminId 后台用户id
     */
    void delPerm(long adminId);

    /**
     * 移除缓存后台用户权限
     * @param adminId 用户id
     * @param time 延时时间
     */
    void delPermAsync(long adminId, long time);

    /**
     * 移除缓存
     * @param permId 权限id
     */
    void delByPermId(long permId);

    /**
     * 移除缓存
     * @param roleId 角色id
     */
    void delByRoleId(long roleId);

    /**
     * 移除缓存
     * @param permId 权限id
     * @param time 延时时间
     */
    void delByPermIdAsync(long permId, long time);

    /**
     * 移除缓存
     * @param roleId 角色id
     * @param time 延时时间              
     */
    void delByRoleIdAsync(long roleId, long time);
}
