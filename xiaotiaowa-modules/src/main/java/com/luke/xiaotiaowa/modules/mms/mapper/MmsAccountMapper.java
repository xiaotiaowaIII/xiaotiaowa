package com.luke.xiaotiaowa.modules.mms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luke.xiaotiaowa.modules.mms.entity.MmsAccountEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-04-01
 */
@Mapper
public interface MmsAccountMapper extends BaseMapper<MmsAccountEntity> {

    /**
     * 分页查询用户
     * @param page 分页
     * @param username 用户名或昵称
     * @param accountNumber 账号
     * @return list
     */
    List<MmsAccountEntity> selectListByPage(IPage<MmsAccountEntity> page, String username, String accountNumber);

}
