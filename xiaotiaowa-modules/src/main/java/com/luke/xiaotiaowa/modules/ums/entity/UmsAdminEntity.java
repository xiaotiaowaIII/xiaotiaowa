package com.luke.xiaotiaowa.modules.ums.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.luke.xiaotiaowa.common.BaseEntity;
import com.luke.xiaotiaowa.valid.IGroups;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.time.LocalDateTime;

/**
 * <p>
 * 后台管理员表
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-22
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("ums_admin")
@Alias("ums_admin")
@ApiModel(value = "UmsAdminEntity", description = "后台管理员表")
public class UmsAdminEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Null(groups = IGroups.ADD.class)
    @NotNull(groups = IGroups.MOD.class)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @NotBlank
    @Length(max = 64)
    @ApiModelProperty("登录名")
    private String username;

    @NotBlank
    @Length(min = 6, max = 18, groups = IGroups.ADD.class)
    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("头像")
    private String icon;

    @Email
    @ApiModelProperty("邮箱")
    private String email;

    @NotBlank
    @Length(max = 100)
    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("备注信息")
    private String note;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @ApiModelProperty("最后登录时间")
    private LocalDateTime loginTime;

    @NotNull
    @ApiModelProperty("帐号启用状态：0->禁用；1->启用")
    private boolean status;

    @ApiModelProperty("乐观锁")
    @Version
    private Long version;
    
    /**
     * 角色名称 逗号隔开
     */
    @TableField(exist = false)
    private String roles;
}
