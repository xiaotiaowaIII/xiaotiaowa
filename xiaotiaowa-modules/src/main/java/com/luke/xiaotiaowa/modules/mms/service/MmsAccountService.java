package com.luke.xiaotiaowa.modules.mms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luke.xiaotiaowa.modules.mms.entity.MmsAccountEntity;

import java.util.List;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-04-01
 */
public interface MmsAccountService {

    /**
     * 分页查询用户
     * @param page 分页
     * @param username 用户名或昵称
     * @param accountNumber 账号
     * @return list
     */
    List<MmsAccountEntity> selectListByPage(IPage<MmsAccountEntity> page, String username, String accountNumber);

    /**
     * 启用
     * @param id 主键
     */
    void activateAccount(long id);

    /**
     * 禁用
     * @param id 主键
     */
    void suspendedAccount(long id);
}
