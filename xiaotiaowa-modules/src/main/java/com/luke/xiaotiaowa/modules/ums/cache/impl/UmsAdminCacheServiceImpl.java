package com.luke.xiaotiaowa.modules.ums.cache.impl;

import com.luke.xiaotiaowa.modules.redis.RedisService;
import com.luke.xiaotiaowa.modules.ums.cache.UmsAdminCacheService;
import com.luke.xiaotiaowa.modules.ums.entity.UmsAdminEntity;
import com.luke.xiaotiaowa.properties.XtwProperties;
import jodd.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.*;

/**
 * 后台用户缓存实现类
 * 
 * @author xiaotiaowa
 * @create 2023/3/29
 */
@Slf4j
@Service
public class UmsAdminCacheServiceImpl implements UmsAdminCacheService {
    
    @Resource
    private RedisService redisService;
    @Resource
    private XtwProperties prop;

    private String buildKey() {
        return prop.getRedis().getDatabase() + ":" + prop.getRedis().getKey().get("admin");
    }
    
    @Override
    public UmsAdminEntity getAdminByUsername(String username) {
        var key = buildKey();
        return (UmsAdminEntity) redisService.hashGet(key, username);
    }

    @Override
    public void setAdmin(UmsAdminEntity admin) {
        redisService.hashSet(buildKey(), admin.getUsername(), admin, 28800);
    }

    @Override
    public void delAdmin(String username) {
        redisService.hashDel(buildKey(), username);
    }

    @Override
    public void delAdminAsync(String username, long time) {
        var namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("thread-delAdminCacheKey-%d").get();
        var singleThreadPool = new ThreadPoolExecutor(1, 5,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());
        singleThreadPool.execute(() -> {
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                log.error("Interrupted！", e);
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
            redisService.hashDel(buildKey(), username);
        });
        singleThreadPool.shutdown();
    }
}
