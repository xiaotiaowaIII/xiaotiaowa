package com.luke.xiaotiaowa.modules.mms.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luke.xiaotiaowa.events.XtwEvents;
import com.luke.xiaotiaowa.modules.mms.entity.MmsAccountEntity;
import com.luke.xiaotiaowa.modules.mms.mapper.MmsAccountMapper;
import com.luke.xiaotiaowa.modules.mms.service.MmsAccountService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-04-01
 */
@Service
public class MmsAccountServiceImpl implements MmsAccountService {
    
    @Resource
    private MmsAccountMapper accountMapper;

    @Override
    public List<MmsAccountEntity> selectListByPage(IPage<MmsAccountEntity> page, String username, String accountNumber) {
        return accountMapper.selectListByPage(page, username, accountNumber);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void activateAccount(long id) {
        var account = selectAccount(id);
        account.setStatus(true);
        accountMapper.updateById(account);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void suspendedAccount(long id) {
        var account = selectAccount(id);
        account.setStatus(false);
        accountMapper.updateById(account);
    }

    private MmsAccountEntity selectAccount(long id) {
        var account = accountMapper.selectById(id);
        XtwEvents.BAD_REQUEST.get().message("用户不存在")
                .ifTrueThrow(ObjectUtil.isNull(account));
        return account;
    }
}
