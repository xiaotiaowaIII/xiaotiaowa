package com.luke.xiaotiaowa.modules.ums.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luke.xiaotiaowa.modules.ums.entity.UmsRoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色角色表 服务类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
public interface UmsRoleService extends IService<UmsRoleEntity> {

    /**
     * 查询用户所拥有的角色
     * @param adminId 用户id
     * @return 角色集
     */
    List<UmsRoleEntity> getRolesByAdminId(long adminId);

    /**
     * 查询角色
     * @param page 分页
     * @param keyword 搜索条件
     * @return 角色集
     */
    List<UmsRoleEntity> selectUmsRole(IPage<UmsRoleEntity> page, String keyword);

    /**
     * 查询角色
     * @param id id
     * @return 角色集
     */
    UmsRoleEntity selectUmsRole(long id);

    /**
     * 新增角色
     * @param role entity
     * @return UmsRoleEntity
     */
    UmsRoleEntity addUmsRole(UmsRoleEntity role);


    /**
     * 编辑角色
     * @param id 主键
     * @param role entity
     * @return UmsRoleEntity
     */
    UmsRoleEntity modUmsRole(long id, UmsRoleEntity role);

    /**
     * 删除角色
     * @param id id
     */
    void delUmsRole(long id);

    /**
     * 启用
     * @param id 主键
     */
    void activateUmsRole(long id);

    /**
     * 禁用
     * @param id 主键
     */
    void suspendedUmsRole(long id);

    /**
     * 查询角色
     * @param name 名称
     * @return UmsRoleEntity
     */
    UmsRoleEntity selectUmsRoleByName(String name);

    /**
     * 分配权限
     * @param roleId 角色id
     * @param perms 权限分类id集
     */
    void assignPerm(long roleId, List<Long> perms);
}
