package com.luke.xiaotiaowa.modules.ums.mapper;

import com.luke.xiaotiaowa.modules.ums.entity.UmsPermCategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 模块分类表 Mapper 接口
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Mapper
public interface UmsPermCategoryMapper extends BaseMapper<UmsPermCategoryEntity> {

    /**
     * 查询所有后台权限分类
     * @return list
     */
    List<UmsPermCategoryEntity> selectAll();

    /**
     * 查询角色所拥有的权限分类id
     * @param roleId 角色id
     * @return list
     */
    List<UmsPermCategoryEntity> selectByRoleId(long roleId);

    /**
     * 查询后台用户所拥有的权限分类id
     * @param adminId 后台用户id
     * @return list
     */
    List<UmsPermCategoryEntity> selectByAdminId(long adminId);
}
