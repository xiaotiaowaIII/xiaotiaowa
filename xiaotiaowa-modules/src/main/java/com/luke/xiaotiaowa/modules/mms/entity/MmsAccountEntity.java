package com.luke.xiaotiaowa.modules.mms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.luke.xiaotiaowa.common.BaseEntity;
import java.io.Serializable;
import java.time.LocalDate;

import com.luke.xiaotiaowa.valid.IGroups;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-04-01
 */
@Getter
@Setter
@TableName("mms_account")
@Alias("mms_account")
@ApiModel(value = "MmsAccountEntity对象", description = "用户信息表")
public class MmsAccountEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Null(groups = IGroups.ADD.class)
    @NotNull(groups = IGroups.MOD.class)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @NotNull(groups = IGroups.MOD.class)
    @ApiModelProperty("账号")
    private String accountNumber;

    @NotBlank
    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("名字")
    private String nickname;

    @NotBlank
    @ApiModelProperty("手机号码")
    private String phone;

    @ApiModelProperty("帐号启用状态:0->禁用；1->启用")
    private Boolean status;

    @ApiModelProperty("头像")
    private String icon;

    @ApiModelProperty("背景")
    private String backgroundImage;

    @ApiModelProperty("二维码")
    private String qrCode;

    @ApiModelProperty("性别：0->未知；1->男；2->女")
    private Integer gender;

    @ApiModelProperty("生日")
    private LocalDate birthday;

    @ApiModelProperty("星座")
    private String constellation;

    @ApiModelProperty("地区信息-国家")
    private String country;

    @ApiModelProperty("地区信息-省份")
    private String province;

    @ApiModelProperty("地区信息-城市")
    private String city;

    @ApiModelProperty("职业")
    private String job;

    @ApiModelProperty("简介")
    private String introduction;

    @ApiModelProperty("学校")
    private String school;

    @ApiModelProperty("入学时间")
    private LocalDate admissionDate;
}
