package com.luke.xiaotiaowa.modules.ums.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 后台用户角色和权限关系表
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Getter
@Setter
@TableName("ums_role_perm_relation")
@ApiModel(value = "UmsRolePermRelationEntity对象", description = "后台用户角色和权限关系表")
public class UmsRolePermRelationEntity {

    private static final long serialVersionUID = 1L;

    
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long roleId;

    private Long permId;


}
