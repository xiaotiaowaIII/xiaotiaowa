package com.luke.xiaotiaowa.modules.ums.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.luke.xiaotiaowa.common.BaseEntity;
import com.luke.xiaotiaowa.valid.IGroups;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * <p>
 * 后台用户角色表
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Getter
@Setter
@TableName("ums_role")
@Alias("ums_role")
@ApiModel(value = "UmsRoleEntity对象", description = "后台用户角色表")
public class UmsRoleEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Null(groups = IGroups.ADD.class)
    @NotNull(groups = IGroups.MOD.class)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @NotBlank
    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("描述")
    private String description;

    @ApiModelProperty("后台用户数量")
    private Integer adminCount;

    @ApiModelProperty("启用状态：0->禁用；1->启用")
    private Boolean status;

    private Integer sort;

    @ApiModelProperty("乐观锁")
    @Version
    private Long version;


}
