package com.luke.xiaotiaowa.modules.ums.service.impl;

import com.luke.xiaotiaowa.modules.ums.entity.UmsAdminRoleRelationEntity;
import com.luke.xiaotiaowa.modules.ums.mapper.UmsAdminRoleRelationMapper;
import com.luke.xiaotiaowa.modules.ums.service.UmsAdminRoleRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台用户和角色关系表 服务实现类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Service
public class UmsAdminRoleRelationServiceImpl extends ServiceImpl<UmsAdminRoleRelationMapper, UmsAdminRoleRelationEntity> implements UmsAdminRoleRelationService {

}
