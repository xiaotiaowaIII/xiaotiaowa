package com.luke.xiaotiaowa.modules.ums.service.impl;

import com.luke.xiaotiaowa.modules.ums.entity.UmsRolePermRelationEntity;
import com.luke.xiaotiaowa.modules.ums.mapper.UmsRolePermRelationMapper;
import com.luke.xiaotiaowa.modules.ums.service.UmsRolePermRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台用户角色和权限关系表 服务实现类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Service
public class UmsRolePermRelationServiceImpl extends ServiceImpl<UmsRolePermRelationMapper, UmsRolePermRelationEntity> implements UmsRolePermRelationService {

}
