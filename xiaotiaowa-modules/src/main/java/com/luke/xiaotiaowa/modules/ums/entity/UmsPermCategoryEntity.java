package com.luke.xiaotiaowa.modules.ums.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.luke.xiaotiaowa.common.BaseEntity;
import com.luke.xiaotiaowa.valid.IGroups;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;

/**
 * <p>
 * 模块分类表
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
@Getter
@Setter
@TableName("ums_perm_category")
@Alias("ums_perm_category")
@ApiModel(value = "UmsPermCategoryEntity对象", description = "模块分类表")
public class UmsPermCategoryEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Null(groups = IGroups.ADD.class)
    @NotNull(groups = IGroups.MOD.class)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @NotBlank
    @ApiModelProperty("模块名称")
    private String name;

    @NotBlank
    @ApiModelProperty("key")
    private String categoryKey;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("乐观锁")
    @Version
    private Long version;

    @ApiModelProperty("权限子类")
    @TableField(exist = false)
    private List<UmsPermEntity> children;
}
