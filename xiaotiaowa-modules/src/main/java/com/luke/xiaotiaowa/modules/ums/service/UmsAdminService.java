package com.luke.xiaotiaowa.modules.ums.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luke.xiaotiaowa.modules.ums.entity.UmsAdminEntity;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * <p>
 * 后台管理员表 服务类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-22
 */
public interface UmsAdminService {

    /**
     * 登录
     * @param username 用户名
     * @param password 密码
     * @return token
     */
    String login(String username, String password);

    /**
     * 刷新token
     * @param oldToken 旧token
     * @return token
     */
    String refreshToken(String oldToken);

    /**
     * 分页查询后台用户
     * @param page 分页
     * @param keyword 用户名或昵称
     * @return list
     */
    List<UmsAdminEntity> selectUmsAdmin(IPage<UmsAdminEntity> page, String keyword);

    /**
     * 通过主键查询单条后台用户数据
     * @param id 主键
     * @return UmsAdminEntity
     */
    UmsAdminEntity selectUmsAdmin(long id);

    /**
     * 获取用户信息
     * @param username 用户名
     * @return UserDetails
     */
    UserDetails loadUserByUsername(String username);

    /**
     * 通过用户名查询单条后台用户数据
     * @param username 用户名
     * @return UmsAdminEntity
     */
    UmsAdminEntity selectUmsAdminByUsername(String username);

    /**
     * 新增后台用户
     * @param umsAdmin entity
     * @return UmsAdminEntity
     */
    UmsAdminEntity addUmsAdmin(UmsAdminEntity umsAdmin);


    /**
     * 编辑后台用户
     * @param id 主键
     * @param umsAdmin entity
     * @return UmsAdminEntity
     */
    UmsAdminEntity modUmsAdmin(long id, UmsAdminEntity umsAdmin);

    /**
     * 删除后台用户
     * @param id id
     */
    void delUmsAdmin(long id);

    /**
     * 启用
     * @param id 主键
     */
    void activateUmsAdmin(long id);

    /**
     * 禁用
     * @param id 主键
     */
    void suspendedUmsAdmin(long id);

    /**
     * 分配角色
     * @param id 主键
     * @param roles 角色集合
     */
    void assignRoles(long id, List<Long> roles);
}
