package com.luke.xiaotiaowa.modules.ums.service;

import com.luke.xiaotiaowa.modules.ums.entity.UmsPermCategoryEntity;

import java.util.List;

/**
 * <p>
 * 模块分类表 服务类
 * </p>
 *
 * @author xiaotiaowa
 * @since 2023-03-24
 */
public interface UmsPermCategoryService {

    /**
     * 查询后台用户所拥有的权限分类
     * @param adminId 后台用户id
     * @return list
     */
    List<UmsPermCategoryEntity> getPermCategoryByAdminId(long adminId);
    

    /**
     * 查询角色所拥有的权限分类
     * @param roleId 角色id
     * @return list
     */
    List<UmsPermCategoryEntity> getPermCategoryByRoleId(long roleId);

    /**
     * 查询所有后台权限分类及子类
     * @return list
     */
    List<UmsPermCategoryEntity> listAll();

    /**
     * 查询权限分类
     * @param id id
     * @return 权限分类集
     */
    UmsPermCategoryEntity selectPermCategory(long id);

    /**
     * 新增权限分类
     * @param permCategory entity
     * @return UmsPermCategoryEntity
     */
    UmsPermCategoryEntity addPermCategory(UmsPermCategoryEntity permCategory);


    /**
     * 编辑权限分类
     * @param id 主键
     * @param permCategory entity
     * @return UmsPermCategoryEntity
     */
    UmsPermCategoryEntity modPermCategory(long id, UmsPermCategoryEntity permCategory);

    /**
     * 删除权限分类
     * @param id id
     */
    void delPermCategory(long id);

}
