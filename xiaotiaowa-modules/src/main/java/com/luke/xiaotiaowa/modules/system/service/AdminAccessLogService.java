package com.luke.xiaotiaowa.modules.system.service;

import com.luke.xiaotiaowa.modules.system.entity.AdminAccessLog;
import org.springframework.data.domain.Page;

/**
 * 后台操作日志接口
 * 
 * @author xiaotiaowa
 * @create 2023/3/30
 */
public interface AdminAccessLogService {

    /**
     * 分页查询操作日志
     * 
     * @param username 用户名
     * @param pageNum 页面
     * @param pageSize 每页数量
     * @return Page<AdminAccessLogEntity>
     */
    Page<AdminAccessLog> getByPage(String username, Integer pageNum, Integer pageSize);

    /**
     * 新增一条操作日志
     * @param accessLog 对象
     * @return AdminAccessLogEntity
     */
    AdminAccessLog save(AdminAccessLog accessLog);

    /**
     * 删除一条操作日志
     * @param id 主键
     */
    void remove(String id);

    /**
     * 修改记录
     * @param accessLog 对象
     * @return AdminAccessLogEntity
     */
    AdminAccessLog update(AdminAccessLog accessLog);
}
