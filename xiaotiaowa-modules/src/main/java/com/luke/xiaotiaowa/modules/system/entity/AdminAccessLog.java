package com.luke.xiaotiaowa.modules.system.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * 后台访问日志
 * 
 * @author xiaotiaowa
 * @create 2023/3/30
 */
@Getter
@Setter
@Document
@ApiModel(value = "AdminAccessLogEntity", description = "后台操作日志")
public class AdminAccessLog {

    @Id
    private String id;
    
    /**
     * 操作描述
     */
    @ApiModelProperty("操作描述")
    private String description;

    /**
     * 操作用户
     */
    @Indexed
    @ApiModelProperty("操作用户")
    private String username;

    /**
     * 操作时间
     */
    @ApiModelProperty("操作时间")
    private LocalDateTime startTime;

    /**
     * 消耗时间
     */
    @ApiModelProperty("消耗时间")
    private Integer spendTime;

    /**
     * 根路径
     */
    @ApiModelProperty("根路径")
    private String basePath;

    /**
     * URI
     */
    @ApiModelProperty("URI")
    private String uri;
    
    /**
     * 请求类型
     */
    @ApiModelProperty("请求类型")
    private String method;

    /**
     * IP地址
     */
    @ApiModelProperty("IP地址")
    private String ip;

    /**
     * 请求参数
     */
    @ApiModelProperty("请求参数")
    private Object parameter;

    /**
     * 请求返回的结果
     */
    @ApiModelProperty("请求返回的结果")
    private Object result;

    /**
     * 狀態
     */
    @Indexed
    @ApiModelProperty("狀態")
    private Boolean status;
}
