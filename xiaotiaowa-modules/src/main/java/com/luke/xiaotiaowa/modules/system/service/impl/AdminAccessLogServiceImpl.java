package com.luke.xiaotiaowa.modules.system.service.impl;

import com.luke.xiaotiaowa.modules.system.entity.AdminAccessLog;
import com.luke.xiaotiaowa.modules.system.repository.AdminAccessLogRepository;
import com.luke.xiaotiaowa.modules.system.service.AdminAccessLogService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * 后台操作实现类
 *
 * @author xiaotiaowa
 * @create 2023/3/30
 */
@Service
public class AdminAccessLogServiceImpl implements AdminAccessLogService {

    @Resource
    private AdminAccessLogRepository accessLogRepository;

    @Override
    public Page<AdminAccessLog> getByPage(String username, Integer pageNum, Integer pageSize) {
        var pageable = PageRequest.of(pageNum - 1, pageSize);
        return Optional.ofNullable(username)
                .map(v ->accessLogRepository.findByUsernameOrderByStartTimeDesc(v, pageable))
                .orElse(accessLogRepository.findAllByOrderByStartTimeDesc(pageable));
    }

    @Override
    public AdminAccessLog save(AdminAccessLog accessLog) {
        return accessLogRepository.save(accessLog);
    }

    @Override
    public void remove(String id) {
        accessLogRepository.deleteById(id);
    }

    @Override
    public AdminAccessLog update(AdminAccessLog accessLog) {
        return accessLogRepository.save(accessLog);
    }
}
