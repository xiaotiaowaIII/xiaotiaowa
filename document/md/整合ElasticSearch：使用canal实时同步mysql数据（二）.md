# 整合ElasticSearch：使用canal实时同步mysql数据（二）
<!-- TOC -->
* [整合ElasticSearch：使用canal实时同步mysql数据（二）](#整合elasticsearch使用canal实时同步mysql数据二)
  * [前言](#前言)
  * [环境](#环境)
  * [部署](#部署)
    * [1. 下载canal](#1-下载canal)
      * [canal 各组件的用途](#canal-各组件的用途)
    * [2. 安装canal](#2-安装canal)
    * [3. 配置mysql](#3-配置mysql)
    * [4. 创建mysql测试表](#4-创建mysql测试表)
    * [5. 配置canal-deployer服务器](#5-配置canal-deployer服务器)
    * [6. 配置canal-adapter客户端](#6-配置canal-adapter客户端)
    * [7. 启动canal-adapter客户端](#7-启动canal-adapter客户端)
    * [8. 同步数据](#8-同步数据)
  * [使用canal-admin](#使用canal-admin)
    * [1. 创建canal_manager数据库, 脚步/conf/canal_manager.sql](#1-创建canal_manager数据库-脚步confcanal_managersql)
    * [2. 修改配置文件/conf/application.yml](#2-修改配置文件confapplicationyml)
    * [3. 启动，/bin/startup.sh](#3-启动binstartupsh)
    * [4. 访问，http://localhost:8089  登录：admin/123456](#4-访问httplocalhost8089-登录admin123456)
<!-- TOC -->
## 前言

> 在上一篇文章中，我们简单介绍了ElasticSearch的一些概念，并讲述了ElasticSearch、Kibana、ik分词插件的安装和使用。
> 本文将继续介绍canal的安装和使用，并结合ElasticSearch进行数据同步。

## 环境

| 应用             | 端口    | 版本     |
|----------------|-------|--------|
| mysql          | 3306  | 8.0.35 |
| ElasticSearch  | 9200  | 8.11.0 |
| Kibana         | 5601  | 8.11.0 |
| canal-deployer | 11111 | 1.1.7  |
| canal-adapter  | 8081  | 1.1.7  |
| canal-admin    | 8089  | 1.1.7  |

## 部署

### 1. 下载canal

> 首先我们需要下载canal的各个组件canal-deployer、canal-adapter、canal-admin，大家可以根据自己需要下载版本，我这里使用的版本是1.1.7版本
> 下载地址：https://github.com/alibaba/canal/releases

#### canal 各组件的用途

- canal-server（canal-deployer）：可以监听到Mysql的binlog，只负责接收数据，不处理数据。
- canal-adapter：会从canal-server中获取数据，然后对数据进行同步，可以同步到Mysql、ElasticSearch等存储中。
- canal-admin：为canal提供整体配置管理、节点运维等面向运维的功能，提供WebUi操作界面。

### 2. 安装canal

> 下载canal的安装包，解压后，进入bin目录，执行startup.sh启动canal。

### 3. 配置mysql
> canal是通过订阅Mysql的binlog来实现数据同步，需开启Mysql的binlog写入功能，并设置binlog-format为ROW模式。
> 修改my.ini文件  binlog_format=row

![mysql_variables.png](../pic/mysql_variables.png)
![mysql_variables1.png](../pic/mysql_variables1.png)
![mysql_variables2.png](../pic/mysql_variables2.png)

### 4. 创建mysql测试表
```sql=
CREATE TABLE `test_product` (
    `id` bigint NOT NULL,
    `name` varchar(200) DEFAULT NULL,
    `sub_title` varchar(255) DEFAULT NULL,
    `description` text,
    `original_price` decimal(10,2) DEFAULT NULL,
    `stock` int DEFAULT NULL,
    `low_stock` int DEFAULT NULL,
    `unit` varchar(16) DEFAULT NULL,
    `weight` decimal(10,2) DEFAULT NULL,
    `keywords` varchar(255) DEFAULT NULL,
    `note` varchar(255) DEFAULT NULL,
    `detail_title` varchar(255) DEFAULT NULL,
    `detail_desc` text,
    `brand_name` varchar(255) DEFAULT NULL,
    `product_category_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

```

### 5. 配置canal-deployer服务器
> 修改配置文件conf/example/instance.properties，配置canal-server的ip和端口，以及mysql的账号密码。运行bin目录下的startup.sh启动服务器。
```
# 需要同步的mysql地址
canal.instance.master.address=127.0.0.1:3306
canal.instance.master.journal.name=
canal.instance.master.position=
canal.instance.master.timestamp=
canal.instance.master.gtid=
# 用于同步数据的数据库账号
canal.instance.dbUsername=root
# 用于同步数据的数据库密码
canal.instance.dbPassword=123456
# 数据库连接编码
canal.instance.connectionCharset = UTF-8
# 需要订阅binlog的表过滤正则表达式
canal.instance.filter.regex=.*\\..*
```

### 6. 配置canal-adapter客户端
> 修改配置文件conf/application.yml
```yaml=
server:
  port: 8081
spring:
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
    time-zone: GMT+8
    default-property-inclusion: non_null

canal.conf:
  mode: tcp # 客户端的模式，可选tcp kafka rocketMQ
 # flatMessage: true # 扁平message开关, 是否以json字符串形式投递数据, 仅在kafka/rocketMQ模式下有效
 # zookeeperHosts: # 对应集群模式下的zk地址
  syncBatchSize: 1000 # 每次同步的批数量
  retries: 0 # 重试次数, -1为无限重试
  timeout: # 同步超时时间, 单位毫秒
  accessKey:
  secretKey:
  consumerProperties:
    # canal tcp consumer
    canal.tcp.server.host: 127.0.0.1:11111 #设置canal-server的地址
    canal.tcp.zookeeper.hosts:
    canal.tcp.batch.size: 500
    canal.tcp.username:
    canal.tcp.password:

  srcDataSources: # 源数据库配置
    defaultDS:
      url: jdbc:mysql://127.0.0.1:3306/xiaotiaowa?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai
      username: root
      password: 123456
  canalAdapters: # 适配器列表
  - instance: example # canal实例名或者MQ topic名
    groups: # 分组列表
    - groupId: g1 # 分组id, 如果是MQ模式将用到该值
      outerAdapters:
      - name: logger # 日志打印适配器
      - name: es8 # ES同步适配器
        key: es8Key1
        hosts: http://127.0.0.1:9200 # ES连接地址
        properties:
          mode: rest # 模式可选transport(9300) 或者 rest(9200)
          security.auth: xiaotiaowa:123456 #  only used for rest mode
          cluster.name: elasticsearch # ES集群名称
```
> 添加配置…/conf/es8/test_product.yml，用于配置MySQL中的表与Elasticsearch中索引的映射关系；
```shell=
ddataSourceKey: defaultDS #源数据源的key，对应上面配置的srcDataSources中的值
destination: example # canal的instance或者mq的topic
groupId: g1
outerAdapterKey: mysqlToEs
esMapping:
  _index: test_product # es的索引名称
  _id: _id
  sql: "SELECT 
        tp.id as _id, 
        tp.name, 
        tp.sub_title, 
        tp.description, 
        tp.original_price, 
        tp.stock, 
        tp.low_stock, 
        tp.unit, 
        tp.weight,
        tp.keywords, 
        tp.note, 
        tp.detail_title, 
        tp.detail_desc, 
        tp.brand_name, 
        tp.product_category_name
    FROM test_product tp"  # sql映射
  commitBatch: 3000
```

### 7. 启动canal-adapter客户端
> 启动bin目录下的startup.sh启动客户端。

### 8. 同步数据
```elasticsearch=
# 创建索引
PUT test_product
{
  "mappings": {
    "properties": {
      "name": {
        "type": "text"
      },
      "sub_title": {
        "type": "text"
      },
      "description": {
        "type": "text"
      },
      "original_price": {
        "type": "double"
      },
      "stock": {
        "type": "integer"
      },
      "low_stock": {
        "type": "integer"
      },
      "unit": {
        "type": "text"
      },
      "weight": {
        "type": "double"
      },
      "keywords": {
        "type": "text"
      },
      "note": {
        "type": "text"
      },
      "detail_title": {
        "type": "text"
      },
      "detail_desc": {
        "type": "text"
      },
      "brand_name": {
        "type": "text"
      },
      "product_category_name": {
        "type": "text"
      }
    }
  }
}
```
```sql=
INSERT INTO xiaotiaowa.test_product (id,name,sub_title,description,original_price,stock,low_stock,unit,weight,keywords,note,detail_title,detail_desc,brand_name,product_category_name) VALUES
	 (1,'华为 HUAWEI P20 ','AI智慧全面屏 6GB +64GB 亮黑色 全网通版 移动联通电信4G手机 双卡双待手机 双卡双待','AI智慧全面屏 6GB +64GB 亮黑色 全网通版 移动联通电信4G手机 双卡双待手机 双卡双待',4288.0,1000,0,'件',0.0,'华为',NULL,NULL,NULL,'华为','手机通讯');

UPDATE test_product SET name = '华为 HUAWEI P20 x' WHERE id = 1;
	
DELETE FROM test_product WHERE id=1;
```
> kibana查看ElasticSearch数据，已同步成功
```elasticsearch=
GET test_product/_search
```

## 使用canal-admin
### 1. 创建canal_manager数据库, 脚步/conf/canal_manager.sql

### 2. 修改配置文件/conf/application.yml
```yaml=
server:
  port: 8089
spring:
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
    time-zone: GMT+8

spring.datasource:
  address: 127.0.0.1:3306
  database: canal_manager
  username: root
  password: 123456
  driver-class-name: com.mysql.jdbc.Driver
  url: jdbc:mysql://${spring.datasource.address}/${spring.datasource.database}?useUnicode=true&characterEncoding=UTF-8&useSSL=false
  hikari:
    maximum-pool-size: 30
    minimum-idle: 1

canal:
  adminUser: admin
  adminPasswd: admin
```

### 3. 启动，/bin/startup.sh

### 4. 访问，http://localhost:8089  登录：admin/123456
