# 整合ElasticSearch：安装（一）
<!-- TOC -->
* [整合ElasticSearch：安装（一）](#整合elasticsearch安装一)
  * [前言](#前言)
  * [环境](#环境)
    * [1. 索引（index）](#1-索引index)
    * [2. 类型（type）](#2-类型type)
    * [3. 文档（document）](#3-文档document)
    * [4. 字段（field）](#4-字段field)
    * [5. 映射（mapping）](#5-映射mapping)
    * [6. 别名（alias）](#6-别名alias)
    * [7. 集群（cluster）](#7-集群cluster)
    * [8. 节点（node）](#8-节点node)
    * [9. 分片（shards）](#9-分片shards)
    * [10.倒排索引（inverted index）](#10倒排索引inverted-index)
  * [部署](#部署)
    * [1. 安装Elasticsearch](#1-安装elasticsearch)
    * [2. 安装Kibana](#2-安装kibana)
    * [3. 创建用户及授权](#3-创建用户及授权)
  * [IK分词器](#ik分词器)
    * [1. 下载](#1-下载)
<!-- TOC -->

## 前言
> Elasticsearch是一个基于Lucene的搜索和分析引擎，提供了一个分布式的实时搜索和分析引擎，
> 可以处理大量的数据。它支持全文搜索、结构化搜索、地理位置搜索等，并且可以提供实时的结果。
> 
> Elasticsearch还可以与其他工具集成，例如Kibana和Logstash，以提供更强大的功能。
> 它具有可扩展性、高可用性和容错性，并且可以在多个节点上进行分布式部署。

## 环境
|                    | 版本   | 
| ---------------------- |--------|
| Elasticsearch            | 8.11.0 |
| kibana                  | 8.11.0 |

### 1. 索引（index）
> 索引是Elasticsearch中最重要的概念，它是Elasticsearch的逻辑存储单元，
> 类似于关系型数据库中的数据库。
> 
> 索引的作用是将数据存储到一个或多个节点上，并提供对这些数据的搜索和分析。

### 2. 类型（type）
> 类型是索引的逻辑概念，类似于关系型数据库中的表。
> 
> 类型是Elasticsearch中用于存储数据的逻辑结构，它定义了文档的结构，并将文档存储到索引中。
> 
> 一个索引可以包含多个类型，每个类型可以包含多个文档。

### 3. 文档（document）
> 类似于关系型数据库中的行, 就是一条json数据。
> 
> 文档是类型中的一个实例，它包含字段和值。

### 4. 字段（field）
> 类似于关系型数据库中的列。
> 
> 字段是文档中的一个属性，它包含一个字段名和值。

### 5. 映射（mapping）
> 类似于关系型数据库中的表结构。
> 
> 映射是Elasticsearch中用于定义文档结构的JSON文档，它定义了字段的类型、是否索引、是否存储等。
> 
> 映射是索引的元数据，它定义了索引中文档的结构。映射可以被索引、搜索和分析使用。

### 6. 别名（alias）
> 类似于关系型数据库中的视图。
> 
> 别名是索引的逻辑概念，它是指向索引的指针。
> 
> 别名可以被索引、搜索和分析使用。
> 
> 别名可以指向一个索引，也可以指向多个索引。

### 7. 集群（cluster）
> 集群是Elasticsearch中用于存储数据的物理结构，它包含一个或多个节点。

### 8. 节点（node）
> 单个 Elasticsearch 服务器。一个或多个节点可以形成一个群集

### 9. 分片（shards）
> 分片（Shard）是数据的逻辑分区，用于在集群中分散数据存储和计算负载。
> 每个索引被分成多个主分片和副本分片，以实现可扩展性和高可用性。
> 主分片是数据的物理分区，而副本分片是用于备份和故障转移的额外 copies。
> 分片的数量可以根据集群的规模和需求进行配置，以平衡存储和性能。
> 通过将数据分散到多个分片上，Elasticsearch可以并行处理查询和索引操作，提高查询性能和整体吞吐量。

### 10.倒排索引（inverted index）
> 倒排索引是Elasticsearch中用于存储文档内容的结构。
> 
> 倒排索引是一种结构，它将每个文档的字段和值存储为一个键值对。
> 
> 键是字段的值，值是文档的ID。
> 
> 倒排索引可以被搜索引擎用来快速查找文档。

## 部署
### 1. 安装Elasticsearch
> 下载地址：https://www.elastic.co/downloads/elasticsearch
> 
> 解压后，在bin目录下执行：./elasticsearch
> 
> 启动成功后，访问http://localhost:9200即可查看Elasticsearch的控制台。
> 
> 首次启动，会打印用户密码

### 2. 安装Kibana
> 下载地址：https://www.elastic.co/downloads/kibana
>  
> 解压后，在bin目录下执行：./kibana
> 
> 启动成功后，访问http://localhost:5601即可查看Kibana的控制台。

### 3. 创建用户及授权
```shell=
> bin/elasticsearch-users useradd xiaotiaw
> bin/elasticsearch-users roles -a superuser xiaotiaowa
> bin/elasticsearch-users roles -a kibana_system xiaotiaowa
```

## IK分词器
### 1. 下载
> 下载地址：https://github.com/medcl/elasticsearch-analysis-ik/releases
> 
> 将ik-analyzer-8.11.0.zip解压复制到elasticsearch/plugins目录下

