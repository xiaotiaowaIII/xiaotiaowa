# 自定义注解+AOP+Redis实现防重复提交
<!-- TOC -->
* [自定义注解+AOP+Redis实现防重复提交](#自定义注解aopredis实现防重复提交)
  * [简介](#简介)
  * [实现思路流程](#实现思路流程)
    * [唯一性](#唯一性)
  * [代码实现](#代码实现)
    * [1.自定义防重复提交注解](#1自定义防重复提交注解)
    * [2.创建AOP切面，实现防重复提交功能](#2创建aop切面实现防重复提交功能)
    * [3.controller接口标记防重复提交注解](#3controller接口标记防重复提交注解)
    * [4.接口测试](#4接口测试)
<!-- TOC -->

## 简介
> 利用自定义注解+AOP+Redis实现springboot的表单防重复提交
 
## 实现思路流程
```mermaid
graph LR
    A[表单提交] --> B[AOP]
    B[AOP] --> C{查redis是否存在有效期内?}
    C -- 是 --> D[提示 频繁操作,请稍后再试]
    C -- 否 --> E[写入redis]
    E --> F[正常提交]
```

### 唯一性
> 存入redis的key生成需要加上用户登录的token

## 代码实现
### 1.自定义防重复提交注解
```java=
import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;
/**
 * 
 * 防重复提交注解
 * 
 * @author xiaotiaowa
 */
@Inherited
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatSubmit {
    
    /**
     * 锁定时间，默认3000毫秒(3秒)
     */
    int interval() default 3000;

    /**
     * 锁定时间单位，默认毫秒
     */
    TimeUnit timeUnit() default TimeUnit.MILLISECONDS;

    /**
     * 提示信息
     */
    String message() default "频繁操作，请稍后再试！";
    
}
```

### 2.创建AOP切面，实现防重复提交功能
```java=
package com.luke.xiaotiaowa.aspect;


import cn.hutool.crypto.SecureUtil;
import cn.hutool.json.JSONUtil;
import com.luke.xiaotiaowa.aspect.annotation.RepeatSubmit;
import com.luke.xiaotiaowa.events.XtwEvents;
import com.luke.xiaotiaowa.modules.redis.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;


/**
 * 实现防重复提交功能
 *
 * @author xiaotiaowa
 */
@Aspect
@Component
@Slf4j
public class RepeatSubmitAspect {

    @Resource
    private RedisService redisService;


    @Before("@annotation(repeatSubmit)")
    public void before(JoinPoint joinPoint, RepeatSubmit repeatSubmit) {
        var attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

        var request = Optional.ofNullable(attributes)
                .map(ServletRequestAttributes::getRequest)
                .orElseThrow(XtwEvents.INTERNAL_SERVER_ERROR);

        //请求参数拼接
        String requestParams = argsArrayToString(joinPoint.getArgs());

        var authorizationHeader = Optional.ofNullable(request)
                .map(v -> v.getHeader("Authorization")).orElse(null);


        var submitKey = Optional.ofNullable(authorizationHeader)
                .filter(String::isBlank)
                .map(v -> {
                    String token = v.replace("Bearer ", "");
                    return SecureUtil.md5(token + ":" + requestParams);
                }).orElse(SecureUtil.md5(request.getRequestURL().toString() + ":" + requestParams));

        //缓存key
        String cacheKey = "xtw:repeat_submit:" + submitKey;

        XtwEvents.BAD_REQUEST.get().message(repeatSubmit.message())
                .ifTrueThrow(null != redisService.get(cacheKey, Object.class));

        redisService.set(cacheKey, "1", repeatSubmit.interval(), repeatSubmit.timeUnit());
    }

    /**
     * 参数拼接
     *
     * @param args 参数数组
     * @return 拼接后的字符串
     */
    private String argsArrayToString(Object[] args) {
        StringBuilder params = new StringBuilder();
        if (args != null) {
            for (Object o : args) {
                if (Objects.nonNull(o) && !isFilterObject(o)) {
                    try {
                        params.append(JSONUtil.toJsonStr(o)).append(" ");
                    } catch (Exception e) {
                        log.error("参数拼接异常:{}", e.getMessage());
                    }
                }
            }
        }
        return params.toString().trim();
    }

    /**
     * 判断是否需要过滤的对象。
     *
     * @param o 对象
     * @return true：需要过滤；false：不需要过滤
     */
    private boolean isFilterObject(final Object o) {
        Class<?> c = o.getClass();
        //如果是数组且类型为文件类型的需要过滤
        if (c.isArray()) {
            return c.getComponentType().isAssignableFrom(MultipartFile.class);
        }
        //如果是集合且类型为文件类型的需要过滤
        else if (Collection.class.isAssignableFrom(c)) {
            Collection collection = (Collection) o;
            for (Object value : collection) {
                return value instanceof MultipartFile;
            }
        }
        //如果是Map且类型为文件类型的需要过滤
        else if (Map.class.isAssignableFrom(c)) {
            Map map = (Map) o;
            for (Object value : map.entrySet()) {
                Map.Entry entry = (Map.Entry) value;
                return entry.getValue() instanceof MultipartFile;
            }
        }
        //如果是文件类型的需要过滤
        return o instanceof MultipartFile || o instanceof HttpServletRequest || o instanceof HttpServletResponse
                || o instanceof BindingResult;
    }
}

```

### 3.controller接口标记防重复提交注解
```java=
@ApiOperation("新增后台用户")
@RepeatSubmit
@PostMapping("admin")
public UmsAdminEntity addUmsAdmin(@Validated({Default.class, IGroups.ADD.class}) @RequestBody UmsAdminEntity umsAdmin) {
    return umsAdminService.addUmsAdmin(umsAdmin);
}
```
