# 整合SpringSecurity和JWT, 用户认证和授权
<!-- TOC -->
* [整合SpringSecurity和JWT, 用户认证和授权](#整合springsecurity和jwt-用户认证和授权)
  * [参考文档](#参考文档)
  * [简介](#简介)
    * [SpringSecurity](#springsecurity)
    * [JWT](#jwt)
  * [登录校验的流程](#登录校验的流程)
  * [登录认证与授权](#登录认证与授权)
    * [相关数据表](#相关数据表)
    * [添加相关依赖](#添加相关依赖)
    * [application.yml添加相关配置](#applicationyml添加相关配置)
    * [创建JWT Token的工具类](#创建jwt-token的工具类)
    * [添加SpringSecurity的java config](#添加springsecurity的java-config)
      * [IgnoreUrlsConfig](#ignoreurlsconfig)
      * [错误返回工具类](#错误返回工具类)
      * [RestfulAccessDeniedHandler](#restfulaccessdeniedhandler)
      * [RestAuthenticationEntryPoint](#restauthenticationentrypoint)
      * [JwtAuthenticationTokenFilter](#jwtauthenticationtokenfilter)
    * [动态权限相关业务接口](#动态权限相关业务接口)
    * [DynamicAccessDecisionManager](#dynamicaccessdecisionmanager)
    * [DynamicSecurityMetadataSource](#dynamicsecuritymetadatasource)
    * [DynamicSecurityFilter](#dynamicsecurityfilter)
    * [需要认证的模块加入configuration](#需要认证的模块加入configuration)
    * [添加AdminUserDetails](#添加adminuserdetails)
    * [service层](#service层)
    * [controller层](#controller层)
  * [postman测试](#postman测试)
<!-- TOC -->

## 参考文档
[参考文档 by macrozheng](https://www.macrozheng.com/mall/architect/mall_arch_04.html#%E6%B7%BB%E5%8A%A0springsecurity%E7%9A%84%E9%85%8D%E7%BD%AE%E7%B1%BB)


## 简介
### SpringSecurity
> Spring Security是Spring家族的一个安全管理框架, 相比于另一个安全框架Shiro, 它具有更丰富的功能。一般中大型项目都是使用SpringSecurity做安全框架, 而Shiro上手比较简单
* 认证: 只有你的用户名或密码正确才能访问某些资源

* 授权: 当前用户具有哪些功能, 将资源进行划分

### JWT
> JWT简称JSON Web Token,也就是通过JSON形式作为Web应用中的令牌,用于在各方之间安全地将信息作为JSON对象传输。在数据传输过程中还可以完成数据加密、签名等相关处理。

> JWT的由，header（标头），payload（有效载荷），singnature（签名），三个部分组成的一个Stirng类型的字符串。可以通过对字符串的编码和解密，完成WEB端的登录认证和授权。

[token解析结果可以通过 https://jwt.io/ 转换](https://jwt.io/)

* header中用于存放签名的生成算法
```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```

* payload中用于存放用户名、token的生成时间和过期时间
```json=
{
  "sub": "1234567890",
  "name": "John Doe",
  "iat": 1516239022
}
```
* signature为以header和payload生成的签名，一旦header和payload被篡改，验证将失败
```json
//secret为加密算法的密钥
String signature = HMACSHA512(base64UrlEncode(header) + "." +base64UrlEncode(payload),secret)
```

## 登录校验的流程
```mermaid
sequenceDiagram

前端系统->>后端系统: 用户名&密码
后端系统-->>前端系统: 响应JWT给前端
前端系统->>后端系统: 访问其他接口，需在header中携带token
    loop 权限检查

        后端系统->后端系统: 对token解析，查询用户所拥有的资源权限

    end
后端系统-->>前端系统: 响应资源信息
```

## 登录认证与授权
### 相关数据表
* ums_admin：后台用户表
* ums_role：后台用户角色表
* ums_admin_role_relation：后台用户与角色关系表
* ums_perm：后台权限表
* ums_perm_category：后台权限分类表
* ums_role_perm_relation：后台角色与权限关系表


### 添加相关依赖
```xml=
<!--SpringSecurity依赖配置-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
<!--JWT(Json Web Token)登录支持-->
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt</artifactId>
    <version>0.9.0</version>
</dependency>
```

### application.yml添加相关配置
```yaml=
jwt:
  tokenHeader: Authorization #JWT存储的请求头
  secret: af721edf2056f1e1d65bac4c633ffd81 #JWT加解密使用的密钥
  expiration: 604800 #JWT的超期限时间(60*60*24*7)
  tokenHead: 'Bearer '  #JWT负载中拿到开头

secure:
  ignored:
    urls: #安全路径白名单
      - /swagger-ui/
      - /swagger-resources/**
      - /**/v2/api-docs
      - /actuator/**
      - /druid/**
      - /auth/**
```

### 创建JWT Token的工具类
> 用于生成和解析token
* generateToken(UserDetails userDetails) :用于根据登录用户信息生成token
* getUserNameFromToken(String token)：从token中获取登录用户的信息
* validateToken(String token, UserDetails userDetails)：判断token是否还有效
```java=
@Slf4j
public class JwtTokenUtil {
    private static final String CLAIM_KEY_USERNAME = "sub";
    private static final String CLAIM_KEY_CREATED = "created";

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    /**
     * 根据负责生成JWT的token
     */
    private String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /**
     * 从token中获取JWT中的负载
     */
    private Claims getClaimsFromToken(String token) {
        Claims claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            log.info("JWT格式验证失败:{}", token);
        }
        return claims;
    }

    /**
     * 生成token的过期时间
     */
    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }

    /**
     * 从token中获取登录用户名
     */
    public String getUserNameFromToken(String token) {
        String username;
        try {
            var claims = getClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    /**
     * 验证token是否还有效
     *
     * @param token       客户端传入的token
     * @param userDetails 从数据库中查询出来的用户信息
     */
    public boolean validateToken(String token, UserDetails userDetails) {
        var username = getUserNameFromToken(token);
        return username.equals(userDetails.getUsername()) && !isTokenExpired(token);
    }

    /**
     * 判断token是否已经失效
     */
    private boolean isTokenExpired(String token) {
        var expiredDate = getExpiredDateFromToken(token);
        return expiredDate.before(new Date());
    }

    /**
     * 从token中获取过期时间
     */
    private Date getExpiredDateFromToken(String token) {
        var claims = getClaimsFromToken(token);
        return claims.getExpiration();
    }

    /**
     * 根据用户信息生成token
     */
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
        claims.put(CLAIM_KEY_CREATED, new Date());
        return generateToken(claims);
    }

    /**
     * 当原来的token没过期时是可以刷新的
     *
     * @param oldToken 带tokenHead的token
     */
    public String refreshHeadToken(String oldToken) {
        if(StrUtil.isEmpty(oldToken)){
            return null;
        }
        String token = oldToken.substring(tokenHead.length());
        if(StrUtil.isEmpty(token)){
            return null;
        }
        //token校验不通过
        var claims = getClaimsFromToken(token);
        if(claims==null){
            return null;
        }
        //如果token已经过期，不支持刷新
        if(isTokenExpired(token)){
            return null;
        }
        //如果token在30分钟之内刚刷新过，返回原token
        if(tokenRefreshJustBefore(token,30*60)){
            return token;
        }else{
            claims.put(CLAIM_KEY_CREATED, new Date());
            return generateToken(claims);
        }
    }

    /**
     * 判断token在指定时间内是否刚刚刷新过
     * @param token 原token
     * @param time 指定时间（秒）
     */
    private boolean tokenRefreshJustBefore(String token, int time) {
        var claims = getClaimsFromToken(token);
        var created = claims.get(CLAIM_KEY_CREATED, Date.class);
        var refreshDate = new Date();
        //刷新时间在创建时间的指定时间内
        return refreshDate.after(created) && refreshDate.before(DateUtil.offsetSecond(created, time));
    }
}
```

### 添加SpringSecurity的java config
```java=
@Configuration
public class CommonSecurityConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public IgnoreUrlsConfig ignoreUrlsConfig() {
        return new IgnoreUrlsConfig();
    }

    @Bean
    public JwtTokenUtil jwtTokenUtil() {
        return new JwtTokenUtil();
    }

    @Bean
    public RestfulAccessDeniedHandler restfulAccessDeniedHandler() {
        return new RestfulAccessDeniedHandler();
    }

    @Bean
    public RestAuthenticationEntryPoint restAuthenticationEntryPoint() {
        return new RestAuthenticationEntryPoint();
    }

    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter(){
        return new JwtAuthenticationTokenFilter();
    }
    
    @ConditionalOnBean(name = "dynamicSecurityService")
    @Bean
    public DynamicAccessDecisionManager dynamicAccessDecisionManager() {
        return new DynamicAccessDecisionManager();
    }

    @ConditionalOnBean(name = "dynamicSecurityService")
    @Bean
    public DynamicSecurityMetadataSource dynamicSecurityMetadataSource() {
        return new DynamicSecurityMetadataSource();
    }

    @ConditionalOnBean(name = "dynamicSecurityService")
    @Bean
    public DynamicSecurityFilter dynamicSecurityFilter(){
        return new DynamicSecurityFilter();
    }
}
```
```java=
@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    private IgnoreUrlsConfig ignoreUrlsConfig;
    @Autowired
    private RestfulAccessDeniedHandler restfulAccessDeniedHandler;
    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;
    @Autowired
    private UmsAdminService adminService;
    @Autowired(required = false)
    private DynamicSecurityService dynamicSecurityService;
    @Autowired(required = false)
    private DynamicSecurityFilter dynamicSecurityFilter;
    
    @Bean
    public UserDetailsService userDetailsService() {
        //获取登录用户信息
        return username -> adminService.loadUserByUsername(username);
    }

    @Bean
    SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = httpSecurity
                .authorizeRequests();
        //不需要保护的资源路径允许访问
        for (String url : ignoreUrlsConfig.getUrls()) {
            registry.antMatchers(url).permitAll();
        }
        //允许跨域请求的OPTIONS请求
        registry.antMatchers(HttpMethod.OPTIONS)
                .permitAll();
        // 任何请求需要身份认证
        registry.and()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                // 关闭跨站请求防护及不使用session
                .and()
                .csrf()
                .disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                // 自定义权限拒绝处理类
                .and()
                .exceptionHandling()
                .accessDeniedHandler(restfulAccessDeniedHandler)
                .authenticationEntryPoint(restAuthenticationEntryPoint)
                // 自定义权限拦截器JWT过滤器
                .and()
                .addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
        //有动态权限配置时添加动态权限校验过滤器
        if(dynamicSecurityService!=null){
            registry.and().addFilterBefore(dynamicSecurityFilter, FilterSecurityInterceptor.class);
        }
        return httpSecurity.build();
    }
}
```
相关说明：
* PasswordEncoder：SpringSecurity定义的用于对密码进行编码及比对的接口，目前使用的是BCryptPasswordEncoder；
* IgnoreUrlsConfig：白名单资源路径配置
* RestfulAccessDeniedHandler：当用户没有访问权限时的处理器，抛出`Forbidden`错误；
* RestAuthenticationEntryPoint：当未登录或token失效时，抛出`Unauthorized`错误；
* JwtAuthenticationTokenFilter：在用户名和密码校验前添加的过滤器，如果有jwt的token，会自行根据token信息进行登录。
* UserDetailsService:SpringSecurity定义的核心接口，用于根据用户名获取用户信息，需要自行实现；
* UserDetails：SpringSecurity定义用于封装用户信息的类（主要是用户信息和权限），需要自行实现；
* DynamicAccessDecisionManager: 动态权限决策管理器，用于判断用户是否有访问权限
* DynamicSecurityMetadataSource: 动态权限数据源，用于获取动态权限规则
* DynamicSecurityFilter：
#### IgnoreUrlsConfig
```java=
@Getter
@Setter
@ConfigurationProperties(prefix = "secure.ignored")
public class IgnoreUrlsConfig {

    private List<String> urls = new ArrayList<>();
    
}
```
#### 错误返回工具类
```java=
public class HttpResponseUtil {
    
    public static void write(HttpServletResponse response, XtwEvent event) throws IOException {
        var responseVo = ResponseVo.builder()
                .code(event.getCode())
                .message(event.getMessage())
                .build();
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Cache-Control","no-cache");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(JSONUtil.parse(responseVo));
        response.getWriter().flush();
    }
}
```
#### RestfulAccessDeniedHandler
```java=
public class RestfulAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) {
        HttpResponseUtil.write(response, XtwEvents.FORBIDDEN.get().message(accessDeniedException.getMessage()));
    }
}
```
#### RestAuthenticationEntryPoint
```java=
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) {
        HttpResponseUtil.write(response, XtwEvents.UNAUTHORIZED.get().message(authException.getMessage()));
    }
}
```
#### JwtAuthenticationTokenFilter
```java=
@Slf4j
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Override
    protected void doFilterInternal(HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain filterChain) 
            throws ServletException, IOException {
        var authHeader = request.getHeader(this.tokenHeader);
        if (StrUtil.isNotBlank(authHeader) && authHeader.startsWith(this.tokenHead)) {
            var authToken = authHeader.substring(this.tokenHead.length());
            var username = jwtTokenUtil.getUserNameFromToken(authToken);
            log.info("checking username:{}", username);
            if (StrUtil.isNotBlank(username) && SecurityContextHolder.getContext().getAuthentication() == null) {
                var userDetails = this.userDetailsService.loadUserByUsername(username);
                if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                    var authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    log.info("authenticated user:{}", username);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        filterChain.doFilter(request, response);
    }
}
```

### 动态权限相关业务接口
```java=
public interface DynamicSecurityService {

    /**
     * 加载资源ANT通配符和资源对应MAP
     * @return map
     */
    Map<String, ConfigAttribute> loadDataSource();
}
```

### DynamicAccessDecisionManager
```java=
public class DynamicAccessDecisionManager implements AccessDecisionManager {
    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
                for (ConfigAttribute configAttribute : configAttributes) {
            //将访问所需资源或用户拥有资源进行比对
            String needAuthority = configAttribute.getAttribute();
            for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
                if (needAuthority.trim().equals(grantedAuthority.getAuthority())) {
                    return;
                }
            }
        }
        throw new AccessDeniedException(XtwEvents.FORBIDDEN.getMessage());
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
```

### DynamicSecurityMetadataSource
```java=
public class DynamicSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    private static Map<String, ConfigAttribute> configAttributeMap = null;
    @Autowired
    private DynamicSecurityService dynamicSecurityService;

    @PostConstruct
    public void loadDataSource() {
        configAttributeMap = dynamicSecurityService.loadDataSource();
    }

    public void clearDataSource() {
        configAttributeMap.clear();
        configAttributeMap = null;
    }

    @Override
    public Collection<ConfigAttribute> getAttributes(Object o) throws IllegalArgumentException {
        if (MapUtil.isEmpty(configAttributeMap)) {
            this.loadDataSource();
        }
        var configAttributes = new ArrayList<ConfigAttribute>();
        //获取当前访问的路径
        var url = ((FilterInvocation) o).getRequestUrl();
        var path = URLUtil.getPath(url);
        var pathMatcher = new AntPathMatcher();
        //获取访问该路径所需资源
        for (String pattern : configAttributeMap.keySet()) {
            for (String pat: pattern.split(",")) {
                if (pathMatcher.match(pat, path)) {
                    configAttributes.add(configAttributeMap.get(pattern));
                }
            }
        }
        // 未设置操作请求权限，返回空集合
        return configAttributes;
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

}
```


### DynamicSecurityFilter
```java=
public class DynamicSecurityFilter extends AbstractSecurityInterceptor implements Filter {

    @Autowired
    private DynamicSecurityMetadataSource dynamicSecurityMetadataSource;
    @Autowired
    private IgnoreUrlsConfig ignoreUrlsConfig;

    @Autowired
    public void setMyAccessDecisionManager(DynamicAccessDecisionManager dynamicAccessDecisionManager) {
        super.setAccessDecisionManager(dynamicAccessDecisionManager);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
                var request = (HttpServletRequest) servletRequest;
        var request = (HttpServletRequest) servletRequest;
        var fi = new FilterInvocation(servletRequest, servletResponse, filterChain);
        //OPTIONS请求直接放行
        if(request.getMethod().equals(HttpMethod.OPTIONS.toString())) {
            fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
            return;
        }
        //白名单请求直接放行
        var pathMatcher = new AntPathMatcher();
        for (var path : ignoreUrlsConfig.getUrls()) {
            if(pathMatcher.match(path, request.getRequestURI())) {
                fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
                return;
            }
        }
        //此处会调用AccessDecisionManager中的decide方法进行鉴权操作
        var token = super.beforeInvocation(fi);
        try {
            fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
        } finally {
            super.afterInvocation(token, null);
        }
    }

    @Override
    public void destroy() {
    }

    @Override
    public Class<?> getSecureObjectClass() {
        return FilterInvocation.class;
    }

    @Override
    public SecurityMetadataSource obtainSecurityMetadataSource() {
        return dynamicSecurityMetadataSource;
    }
}

```

### 需要认证的模块加入configuration
```java=
@Configuration
public class AdSecurityConfig {

    @Autowired
    private UmsAdminService adminService;
    @Autowired
    private UmsPermService permService;

    @Bean
    public UserDetailsService userDetailsService() {
        //获取登录用户信息
        return username -> adminService.loadUserByUsername(username);
    }

    @Bean
    public DynamicSecurityService dynamicSecurityService() {
        return () -> {
            List<UmsPermEntity> perms = permService.getAll();
            Map<String, ConfigAttribute> map = new ConcurrentHashMap<>(perms.size());
            for (var perm : perms) {
                map.put(perm.getUrl(), new org.springframework.security.access.SecurityConfig(perm.getId() + ":" + perm.getName()));
            }
            return map;
        };
    }
}
```

### 添加AdminUserDetails
```java=
public class AdminUserDetails implements UserDetails {

    /**
     * 后台用户
     */
    private final UmsAdminEntity umsAdmin;

    /**
     * 用戶所拥有的权限
     */
    private final List<UmsPermEntity> perms;

    public AdminUserDetails(UmsAdminEntity umsAdmin, List<UmsPermEntity> perms) {
        this.umsAdmin = umsAdmin;
        this.perms = perms;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return perms.stream()
                .map(role -> new SimpleGrantedAuthority(role.getId() + ":" + role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return umsAdmin.getPassword();
    }

    @Override
    public String getUsername() {
        return umsAdmin.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return umsAdmin.getStatus().equals(1);
    }
}
```

### service层
```java=
@Slf4j
@Service
public class UmsAdminServiceImpl implements UmsAdminService {

    @Resource
    private UmsAdminMapper umsAdminMapper;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public String login(String username, String password) {
        String token = null;
        //密码需要客户端加密后传递
        var userDetails = loadUserByUsername(username);
        if(!passwordEncoder.matches(password,userDetails.getPassword())){
            throw XtwEvents.BAD_REQUEST.get().message("密码不正确");
        }
        if(!userDetails.isEnabled()){
            throw XtwEvents.BAD_REQUEST.get().message("帐号已被禁用");
        }
        var authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        token = jwtTokenUtil.generateToken(userDetails);
        return token;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        //获取用户信息
        var admin = selectUmsAdminByUsername(username);
        return Optional.ofNullable(admin)
                .map(v -> {
                    var perms = umsPermService.getPerms(v.getId());
                   return new AdminUserDetails(admin, perms);
                })
                .orElseThrow(() -> new UsernameNotFoundException("用户名或密码错误"));
    }

    @Override
    public UmsAdminEntity selectUmsAdminByUsername(String username) {
        return umsAdminMapper.selectByUsername(username);
    }
}
```

### controller层
```java=
@Api(tags = "后台用户管理")
@RestController
@RequestMapping("auth")
public class AuthController {

    @Resource
    private UmsAdminService umsAdminService;
    
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    
    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @ApiOperation(value = "登录")
    @PostMapping
    public Map<String, String> login(@Validated @RequestBody UmsAdminLoginParam umsAdminLoginParam) {
        var token = umsAdminService.login(umsAdminLoginParam.getUsername(), umsAdminLoginParam.getPassword());
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", token);
        tokenMap.put("tokenHead", tokenHead);
        return tokenMap;
    }
}
```

## postman测试
```java=
{
  "password": "123456",
  "username": "xiaotiaowa"
}
```
```java=
{
    "code": 200,
    "message": "success",
    "data": {
        "tokenHead": "Bearer ",
        "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ4aWFvdGlhb3dhIiwiY3JlYXRlZCI6MTY3OTY0MzEzODQ2NywiZXhwIjoxNjgwMjQ3OTM4fQ.jVmZqQvUPcQsTXM7jN8zYPRpFCKw3OiA2OL2JNBsIlruqkHVDi5FR1I4hsBMP9kwGKwH4nx-Un-fBzpnIPOgmg"
    }
}
```
```java=
{
    "code": 403,
    "message": "抱歉，您没有访问权限"
}
```
