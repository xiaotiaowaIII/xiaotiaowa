# 整合Redis实现缓存功能
<!-- TOC -->
* [整合Redis实现缓存功能](#整合redis实现缓存功能)
  * [简介](#简介)
  * [下载地址](#下载地址)
  * [SpringBoot整合](#springboot整合)
    * [引入依赖](#引入依赖)
    * [配置文件](#配置文件)
    * [创建Redis的java config](#创建redis的java-config)
    * [创建Redis接口并实现一些常用的操作](#创建redis接口并实现一些常用的操作)
    * [创建admin的缓存接口并实现功能](#创建admin的缓存接口并实现功能)
    * [业务代码调用](#业务代码调用)
      * [查询](#查询)
      * [新增](#新增)
      * [修改](#修改)
      * [删除](#删除)
    * [测试](#测试)
  * [遇到的问题](#遇到的问题)
    * [疑似序列化错误](#疑似序列化错误)
    * [缓存穿透](#缓存穿透)
      * [解决：](#解决)
    * [Redis与Mysql数据一致性](#redis与mysql数据一致性)
<!-- TOC -->

## 简介
> Redis是一个高性能(NOSQL)的key-value数据库,键值(Key-Value)存储数据库

> 它包含以下基本数据类型：

| 类型   | 说明     |
| ------ | -------- |
| String | 字符串   |
| list   | 链表     |
| hash   | 哈希     |
| set    | 集合     |
| zset   | 有序集合 |

> 该文档讲述SpringBoot如何整合Redis，涉及如何保证数据一致性、避免缓存雪崩、缓存击穿及缓存穿透等问题

## 下载地址
https://github.com/MicrosoftArchive/redis/releases

## SpringBoot整合
### 引入依赖
> 在springBoot2.X中，默认使用了 `Lettuce`，在这排除掉`Lettuce`，改用`jedis`
```xml=
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
    <exclusions>
        <!-- 去掉对 Lettuce 的依赖，因为 Spring Boot 优先使用 Lettuce 作为 Redis 客户端 -->
        <exclusion>
            <groupId>io.lettuce</groupId>
            <artifactId>lettuce-core</artifactId>
        </exclusion>
    </exclusions>
</dependency>
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
</dependency>

<!-- Spring Data Redis 默认使用 Jackson 作为 JSON 序列化的工具 -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
</dependency>

<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.61</version>
</dependency>
```

###  配置文件
```yaml=
spring:
  redis:
    host: 127.0.0.1
    port: 6379
    password: 123456 # Redis 服务器密码，默认为空。生产中，一定要设置 Redis 密码！
    database: 0 # Redis 数据库号，默认为 0 。
    timeout: 3000 # Redis 连接超时时间，单位：毫秒。
    # 对应 RedisProperties.Jedis 内部类
    jedis:
      pool:
        max-active: 8 # 连接池最大连接数，默认为 8 。使用负数表示没有限制。
        max-idle: 8 # 默认连接数最小空闲的连接数，默认为 8 。使用负数表示没有限制。
        min-idle: 0 # 默认连接池最小空闲的连接数，默认为 0 。允许设置 0 和 正数。
        max-wait: -1 # 连接池最大阻塞等待时间，单位：毫秒。默认为 -1 ，表示不限制。

```

### 创建Redis的java config
```java=
@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        // 创建 RedisTemplate 对象
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        // 设置 RedisConnection 工厂
        template.setConnectionFactory(factory);

        // 使用 String 序列化方式，序列化 KEY 。
        template.setKeySerializer(RedisSerializer.string());

        // 使用 JSON 序列化方式（库是 Jackson ），序列化 VALUE 。
        template.setValueSerializer(RedisSerializer.json());
        return template;
    }
}
```

### 创建Redis接口并实现一些常用的操作
> 暂时只举例几种操作
```java=
public interface RedisService {

    /**
     * 保存（会过期）
     * @param key key
     * @param value 值
     * @param time 有效时长
     */
    void set(String key, Object value, long time);

    /**
     * 保存（不会过期）
     * @param key key
     * @param value 值

     */
    void set(String key, Object value);

    /**
     * 获取
     * @param key key
     * @param clazz 转换对象
     * @return Object
     */
    <T> T get(String key, Class<T> clazz);

    /**
     * 删除
     *
     * @param key key
     * @return Boolean
     */
    String del(String key);

    /**
     * 设置过期时间
     *
     * @param key  key
     * @param time 过期时长
     * @return Boolean
     */
    String expire(String key, long time);
    
}
```
```java=
@Service
public class RedisServiceImpl implements RedisService{

    @Resource(name = "redisTemplate")
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private ValueOperations<String, String> operations;


    @Override
    public void set(String key, Object value, long time) {
        operations.set(key, JsonUtils.toJsonString(value), time, TimeUnit.SECONDS);
    }

    @Override
    public void set(String key, Object value) {
        operations.set(key, JsonUtils.toJsonString(value));
    }

    @Override
    public <T> T get(String key, Class<T> clazz) {
        var value = operations.get(key);
        return Optional.ofNullable(value)
                .map(v -> JsonUtils.parseObject(value, clazz))
                .orElse(null);
    }

    @Override
    public String del(String key) {
        return operations.getAndDelete(key);
    }

    @Override
    public String expire(String key, long time) {
        return operations.getAndExpire(key, time, TimeUnit.SECONDS);
    }
    
}
```
> Json工具类，这里引用的是fastjson的依赖包
```java=
public class JsonUtils {

    public static  <T> T parseObject(String text, Class<T> clazz) {
        return JSON.parseObject(text, clazz);
    }

    public static String toJsonString(Object javaObject) {
        return JSON.toJSONString(javaObject);
    }

}

```

### 创建admin的缓存接口并实现功能
```java=
public interface UmsAdminCacheService {
    
    /**
     * 获取后台用户
     * @param username 用户名
     * @return UmsAdminEntity
     */
    UmsAdminEntity getAdminByUsername(String username);

    /**
     * 缓存后台用户信息
     * @param admin 用户信息
     */
    void setAdmin(UmsAdminEntity admin);

    /**
     * 移除后台用户信息的缓存
     * @param username 用户名
     */
    void delAdmin(String username);
}
```
```java=
@Service
public class UmsAdminCacheServiceImpl implements UmsAdminCacheService {
    
    @Resource
    private RedisService redisService;
    @Autowired
    private XtwProperties prop;

    private String buildKey(String value) {
        return prop.getRedis().getDatabase() + ":" + String.format(prop.getRedis().getKey().get("admin"), value);
    }
    
    @Override
    public UmsAdminEntity getAdminByUsername(String username) {
        return redisService.get(buildKey(username), UmsAdminEntity.class);
    }

    @Override
    public void setAdmin(UmsAdminEntity admin) {
        // 设置过期时间为8个小时
        redisService.set(buildKey(admin.getUsername()), admin, 8*60*60);
    }

    @Override
    public void delAdmin(String username) {
        redisService.del(buildKey(username));
    }
}
```

### 业务代码调用
#### 查询
```java=
public UmsAdminEntity selectUmsAdminByUsername(String username) {
    // 查缓存，没有则再查数据库
    var adminCache = adminCacheService.getAdminByUsername(username);
    return Optional.ofNullable(adminCache)
            .orElseGet(() -> {
                var admin = umsAdminMapper.selectByUsername(username);
                if (ObjectUtil.isNotNull(admin)) {
                    adminCacheService.setAdmin(admin);
                }
                return admin;
            });
}
```
#### 新增
```java=
public UmsAdminEntity addUmsAdmin(UmsAdminEntity umsAdmin) {
    var admin = selectUmsAdminByUsername(umsAdmin.getUsername());
    XtwEvents.BAD_REQUEST.get().message("用户名已存在").ifTrueThrow(ObjectUtil.isNotNull(admin));
    umsAdmin.setPassword(passwordEncoder.encode(umsAdmin.getPassword()));
    umsAdminMapper.insert(umsAdmin);
    // 加入缓存
    adminCacheService.setAdmin(umsAdmin);
    return umsAdmin;
}
```

#### 修改
```java=
public UmsAdminEntity modUmsAdmin(long id, UmsAdminEntity umsAdmin) {
        // 删除缓存
        adminCacheService.delAdmin(umsAdmin.getUsername());
        umsAdmin.setId(id);
        var oldAdmin = selectUmsAdmin(id);
        var adminByName = selectUmsAdminByUsername(umsAdmin.getUsername());
        XtwEvents.BAD_REQUEST.get().message("用户名已存在")
                .ifTrueThrow(ObjectUtil.isNotNull(adminByName) && !adminByName.getId().equals(umsAdmin.getId()));
        umsAdmin.setPassword(passwordEncoder.encode(umsAdmin.getPassword()));
        umsAdmin.setVersion(oldAdmin.getVersion());
         // 更新数据库
        umsAdminMapper.updateById(umsAdmin);
        // 删除缓存
        adminCacheService.delAdminAsync(umsAdmin.getUsername(), 2000);
        return umsAdmin;
    }
```

#### 删除
```java=
public void delUmsAdmin(long id) {
    var admin = selectUmsAdmin(id);
    // 删除缓存
    adminCacheService.delAdmin(admin.getUsername());
    umsAdminMapper.deleteById(admin);
}
```

### 测试
> 成功写入缓存

![输入图片说明](document/pic/image.png)


## 遇到的问题
### 疑似序列化错误
> Could not read JSON: Illegal character ((CTRL-CHAR, code 0)): only regular white space (\r, \n, \t) is allowed between tokens

> 一直在查是不是redis序列化的问题，结果是因为set缓存的时候，传参错了
```java=
// 错误的
public void set(String key, Object value, long time) {
    operations.set(key, JSONUtil.toJsonStr(value), time);
}
```
```java=
// 正确的应该是要带TimeUnit
public void set(String key, Object value, long time) {
    operations.set(key, JsonUtils.toJsonString(value), time, TimeUnit.SECONDS);
}
```

### 缓存穿透
> 问题描述：查询不存在缓存中的数据，每次请求都会打到DB，就像缓存不存在一样，这样数据库压力会增大

#### 解决：
* 如果将查到null的数据，就将null缓存到Redis中，这样能解决问题，但是弊端是可能会有很多空缓存，治标不治本
* 布隆过滤器：
1. 空间效率高，占用空间小
2. 查询时间短

> 在走缓存之前先从布隆过滤器中判断这个 key 是否存在

### Redis与Mysql数据一致性
> 因为mysql和redis主从节点数据不是实时同步的，同步数据需要时间，当在并发的环境下就有可能出现数据不一致

> 采用延时双删策略
```java=
del_cache(key); #删除redis缓存数据
update_db(object); #更新数据库数据
logic_sleep(time); #当前逻辑延时执行
del_cache(key); #删除redis缓存数据
```

* 延时双删并非强一致性
```java=
public void delAdminAsync(String username, long time) {
    var namedThreadFactory = new ThreadFactoryBuilder()
            .setNameFormat("thread-delAdminCacheKey-%d").get();
    var singleThreadPool = new ThreadPoolExecutor(1, 1,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());
    singleThreadPool.execute(() -> {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        redisService.hashDel(buildKey(), username);
    });
    singleThreadPool.shutdown();
}
```
```java=
private void delayDoubleDelCache(String username, Runnable runnable) {
    adminCacheService.delAdmin(username);
    runnable.run();
    adminCacheService.delAdminAsync(username, 1000);
}
```
```java=
// 删除缓存
public UmsAdminEntity modUmsAdmin(long id, UmsAdminEntity umsAdmin) {
    // 删除缓存
    adminCacheService.delAdmin(umsAdmin.getUsername());
    delayDoubleDelCache(umsAdmin.getUsername(), () -> {
        umsAdmin.setId(id);
        var oldAdmin = selectUmsAdmin(id);
        var adminByName = selectUmsAdminByUsername(umsAdmin.getUsername());
        XtwEvents.BAD_REQUEST.get().message("用户名已存在")
                .ifTrueThrow(ObjectUtil.isNotNull(adminByName) && !adminByName.getId().equals(umsAdmin.getId()));
        umsAdmin.setPassword(passwordEncoder.encode(umsAdmin.getPassword()));
        umsAdmin.setVersion(oldAdmin.getVersion());
        umsAdmin.setPassword(oldAdmin.getPassword());
        // 更新数据库
        umsAdminMapper.updateById(umsAdmin);
    });
    return umsAdmin;
}
```



