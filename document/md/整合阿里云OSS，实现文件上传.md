# 整合阿里云OSS，实现文件上传
<!-- TOC -->
* [整合阿里云OSS，实现文件上传](#整合阿里云oss实现文件上传)
  * [简介](#简介)
    * [阿里云控制台](#阿里云控制台)
  * [SpringBoot整合](#springboot整合)
    * [引入依赖](#引入依赖)
    * [添加配置](#添加配置)
    * [oss上传操作](#oss上传操作)
    * [controller](#controller)
    * [限制文件上传大小](#限制文件上传大小)
<!-- TOC -->

## 简介
> 对象存储服务（Object Storage Service，简称OSS）


### 阿里云控制台
https://oss.console.aliyun.com/overview

## SpringBoot整合
### 引入依赖
```xml=
<dependency>
    <groupId>com.aliyun.oss</groupId>
    <artifactId>aliyun-sdk-oss</artifactId>
    <version>3.15.1</version>
</dependency>
```

### 添加配置
```yaml=
aliyun:
  oss:
    file:
      endpoint: oss-cn-guangzhou.aliyuncs.com
      keyid: LTAI5tNnWDos*********
      keysecret: yYMQb3jecCZFVyo**********
      bucketname: xiaotiaowa
```

### oss上传操作
```java=
public interface OssService {

    /**
     * 上传文件
     * @param file 文件
     * @return 访问url
     */
    String upload(MultipartFile file);
}
```
```java=
@Service
public class OssServiceImpl implements OssService {

    @Value("${aliyun.oss.file.endpoint}")
    private String endpoint;
    @Value("${aliyun.oss.file.keyid}")
    private String keyId;
    @Value("${aliyun.oss.file.keysecret}")
    private String keySecret;
    @Value("${aliyun.oss.file.bucketname}")
    private String bucketName;

    @Override
    public String upload(MultipartFile file) {
        OSS ossClient = null;
        try {
            var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            ossClient = new OSSClientBuilder().build(endpoint, keyId, keySecret);
            var is = file.getInputStream();
            var filename = file.getOriginalFilename();
            filename = UUID.randomUUID().toString().replaceAll("-", "") + filename;
            var uploadFilename = LocalDate.now().format(formatter) + "/" + filename;
            ossClient.putObject(bucketName, uploadFilename, is);
            return "https://" + bucketName + "." + endpoint + "/" + uploadFilename;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (null != ossClient) {
                ossClient.shutdown();
            }
        }
    }
}

```

### controller
```java=
@Api(tags = "文件操作")
@RestController
@RequestMapping("file")
public class FileController {
    
    @Resource
    private OssService ossService;

    @ApiOperation(value = "上传文件")
    @PostMapping("upload")
    public String upload(@ApiParam(name = "file", value = "文件", required = true) @RequestParam("file") MultipartFile file) {
        return ossService.upload(file);
    }
}
```

### 限制文件上传大小
```yaml=
spring:
  servlet:
    multipart:
      max-file-size: 15MB
      max-request-size: 15MB
```
