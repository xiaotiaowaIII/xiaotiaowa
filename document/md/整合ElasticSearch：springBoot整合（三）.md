# 整合ElasticSearch：springBoot整合（三）
<!-- TOC -->
* [整合ElasticSearch：springBoot整合（三）](#整合elasticsearchspringboot整合三)
  * [环境](#环境)
  * [引入依赖](#引入依赖)
  * [修改application.yml](#修改applicationyml)
  * [创建EsTestProduct类](#创建estestproduct类)
  * [创建TestProductRepository接口](#创建testproductrepository接口)
  * [创建TestProductController类](#创建testproductcontroller类)
<!-- TOC -->[TOC]

## 环境
|               | 版本      | 
|---------------|---------|
| SpringBoot    | 2.7.9   |
| openJDK       | 11.0.21 |
| Elasticsearch | 8.11.0  |

## 引入依赖
```xml=
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-elasticsearch</artifactId>
</dependency>
```

## 修改application.yml
```yaml=
spring:
  data:
    elasticsearch:
      repositories:
        enabled: true
  elasticsearch:
    uris: localhost:9200
    username: xiaotiaowa
    password: 123456
    # 连接超时时间
    connection-timeout: 10000
    # 连接请求超时时间
    socketTimeout: 30000
```

## 创建EsTestProduct类
```java=
@Document(indexName = "test_product")
@Data
@EqualsAndHashCode
@Setting(shards = 1, replicas = 0)
public class EsTestProduct implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String name;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String subTitle;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String keywords;
    private BigDecimal originalPrice;
    private Integer stock;
    private Integer lowStock;
    private String unit;
    private BigDecimal weight;
    private String note;
    private String detailTitle;
    private String detailDesc;
    @Field(type = FieldType.Keyword)
    private String brandName;
    @Field(type = FieldType.Keyword)
    private String productCategoryName;
}
```

## 创建TestProductRepository接口
```java=
public interface TestProductRepository extends ElasticsearchRepository<TestProduct, String> {
}
```

## 创建TestProductController类
