# 整合SonarQube源代码检测工具
<!-- TOC -->
* [整合SonarQube源代码检测工具](#整合sonarqube源代码检测工具)
  * [前言](#前言)
  * [Docker安装](#docker安装)
    * [拉取Docker镜像](#拉取docker镜像)
    * [运行镜像](#运行镜像)
    * [访问sonarqube](#访问sonarqube)
    * [安装插件（中文插件和findbugs）](#安装插件中文插件和findbugs)
    * [对一个spring boot项目进行扫描](#对一个spring-boot项目进行扫描)
    * [执行sonarqube检测](#执行sonarqube检测)
    * [检测结果](#检测结果)
<!-- TOC -->

## 前言
> SonarQube是一个开源的代码质量管理平台，提供了代码质量管理的各种功能，例如静态检查、代码度量、代码审查等等。

[SonarQube官网](https://docs.sonarsource.com/sonarqube/latest/setup-and-upgrade/install-the-server/introduction/)

## Docker安装
> 环境：macOS 14.1 + Docker 24.0.6 + SonarQube

> 使用数据库：PostgreSQL 

### 拉取Docker镜像
```shell=
# 拉取sonarqube镜像
> docker pull sonarqube

#拉取PostgreSQL镜像
> docker pull postfres:9.6.23
```

### 运行镜像
```shell=
#运行postgres数据库
> docker run --name postgresqldb --restart=always \
-p 5432:5432 \
-e POSTGRES_USER=root \
-e POSTGRES_PASSWORD=123456 \
-d postgres:9.6.23 \

# 进入postgres容器，创建用户名和密码
> docker exec -it postgresqldb bash
> /# psql -U root -W

> create user sonar with password 'sonar';
> create database sonar owner sonar;
> grant all privileges on database sonar to sonar;

#运行sonarqube容器
> docker run -d --name sonarqube \
--restart=always \
-p 9000:9000 \
-e sonar.jdbc.username=sonar \
-e sonar.jdbc.password=sonar \
-e sonar.jdbc.url=jdbc:postgresql://127.0.0.1:5432/sonar \
sonarqube:latest 
```

### 访问sonarqube
> http:localhost:9000  默认管理员和密码admin/admin

### 安装插件（中文插件和findbugs）
![img.png](../pic/chinese.png)
![img.png](../pic/findbugs.png)
> 当状态显示为install pending时，插件已安装完成，重启服务即可

### 对一个spring boot项目进行扫描
> 在pom.xml添加相关配置
```xml=
<properties>
    <sonar.host.url>http://localhost:9000/</sonar.host.url>
    <sonar.login>admin</sonar.login>
    <sonar.password>123456</sonar.password>
    <sonar.inclusions>**/*.java,**/*.xml</sonar.inclusions>
</properties>
```
> 引入sonarqube插件依赖
```xml=
<plugin>
    <groupId>org.sonarsource.scanner.maven</groupId>
    <artifactId>sonar-maven-plugin</artifactId>
    <version>3.7.0.1746</version>
</plugin>
```

### 执行sonarqube检测
![img.png](../pic/sonar-mvn.png)

### 检测结果
![img.png](../pic/sonar-final.png)
