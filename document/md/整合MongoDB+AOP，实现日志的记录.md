# 整合MongoDB+AOP，实现日志的记录
[toc]

## 简介
### MongoDB
> Mongodb是为快速开发互联网Web应用而构建的数据库系统，其数据模型和持久化策略就是为了构建高读/写吞吐量和高自动灾备伸缩性的系统。

> 旨在 为 WEB 应用提供可扩展的高性能数据存储解决方案。将数据存储为一个文档（类似JSON对象），数据结构由键值（key=>value）对组成支持丰富的查询表达，可以设置任何属性的索引，也可以嵌套文档来表示层次关系，并存储诸如数组之类的结构。

### AOP
> AOP(Aspect-Oriented Programming)，面向方面编程。
> 使用“横切”技术，作用在于分离系统中的各种关注点，常用于：权限认证、日志、事务处理

## MongoDB介绍
### 概念
| SQL概念     | MongoDB概念 | 解释/说明                           |
| ----------- | ----------- | ----------------------------------- |
| database    | database    | 数据库                              |
| table       | collection  | 数据库表/集合                       |
| row         | document    | 数据记录行/文档                     |
| column      | field       | 数据字段/域                         |
| index       | index       | 索引                                |
| primary key | primary key | 主键,MongoDB自动将_id字段设置为主键 |

### 数据类型
| 数据类型           | 描述                                                                                                       |
| ------------------ | ---------------------------------------------------------------------------------------------------------- |
| String             | 字符串。存储数据常用的数据类型。在 MongoDB 中，UTF-8 编码的字符串才是合法的。                              |
| Integer            | 整型数值。用于存储数值。根据你所采用的服务器，可分为 32 位或 64 位。                                       |
| Boolean            | 布尔值。用于存储布尔值（真/假）。                                                                          |
| Double             | 双精度浮点值。用于存储浮点值。                                                                             |
| Min/Max keys       | 将一个值与 BSON（二进制的 JSON）元素的最低值和最高值相对比。                                               |
| Arrays             | 用于将数组或列表或多个值存储为一个键。                                                                     |
| Timestamp          | 时间戳。记录文档修改或添加的具体时间。                                                                     |
| Object             | 用于内嵌文档。                                                                                             |
| Null               | 用于创建空值。                                                                                             |
| Symbol             | 符号。该数据类型基本上等同于字符串类型，但不同的是，它一般用于采用特殊符号类型的语言。                     |
| Date               | 日期时间。用 UNIX 时间格式来存储当前日期或时间。你可以指定自己的日期时间：创建 Date 对象，传入年月日信息。 |
| Object ID          | 对象 ID。用于创建文档的 ID。                                                                               |
| Binary Data        | 二进制数据。用于存储二进制数据。                                                                           |
| Code               | 代码类型。用于在文档中存储 JavaScript 代码。                                                               |
| Regular expression | 正则表达式类型。用于存储正则表达式。                                                                       |

### 🛠️安装和使用
#### 1.下载
https://www.mongodb.com/try/download/community

#### 2.启动
> mongo.exe 客户端启动
> mongod.exe 服务的运行程序

#### 3.在安装路径下创建data\db和data\log两个文件夹

#### 4.在安装路径下创建mongod.cfg配置文件
```yaml=
systemLog:
    destination: file
    path: D:\developer\env\MongoDB\data\log\mongod.log
storage:
    dbPath: D:\developer\env\MongoDB\data\db
```

#### 5.安装为服务（运行命令需要用管理员权限）
```shell=
D:\java\MongoDB\bin\mongod.exe --config "D:\java\MongoDB\mongod.cfg" --install
```

#### 6.服务相关命令
```shell=
启动服务：net start MongoDB
关闭服务：net stop MongoDB
移除服务：D:\developer\env\MongoDB\bin\mongod.exe --remove
```

#### 7.下载客户端程序
[https://robomongo.org/](https://robomongo.org/)

#### 8.连接
> 打开robo3t.exe并连接到localhost:27017

#### 9.linux&docker安装
```shell=
#拉取镜像 
docker pull mongo:latest

#创建和启动容器 
docker run -d --restart=always -p 27017:27017 --name mymongo -v /data/db:/data/db -d mongo

#进入容器 
docker exec -it mymongo/bin/bash 

#使用MongoDB客户端进行操作 
mongo 

> show dbs #查询所有的数据库 
admin 0.000GB 
config 0.000GB 
local 0.000GB 
```

#### 常用操作
```shell=
1、 Help查看命令提示 
> db.help();
2、 切换/创建数据库
> use test
如果数据库不存在，则创建数据库，否则切换到指定数据库
3、 查询所有数据库 
> show dbs;
4、 删除当前使用数据库 
> db.dropDatabase();
5、 查看当前使用的数据库 
> db.getName();
6、 显示当前db状态 
> db.stats();
7、 当前db版本 
> db.version();
8、 查看当前db的链接机器地址 
> db.getMongo();
```
```shell=
## 数据库操作
1、创建数据库
> use test
> db.article.insert({name:'test'})
2、列出所有数据库
> shwo dbs
3、删除数据库
> db.dropDatabase()

## 集合操作
1、创建集合
> use test
> db.createCollection("xtw")
2、列出所有集合
> show collections
3、删除集合
> db.xtw.drop()

## 文档操作
1、插入
> db.xtw.insert(document)
2.查找
> db.xtw.find({})
3.更新
> db.xtw.update(document)
> db.xtw.save(document)
4.删除
> db.xtw.remove({})
```

## SpringBoot整合MongoDB
### 引入依赖
```xml=
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-mongodb</artifactId>
</dependency>
```

### 修改配置文件
```yaml=
spring:
  data:
    mongodb:
      host: localhost
      port: 27017
      database: mall-port
      auto-index-creation: true # 默认为false，即不会自动创建索引，
```

### 创建document，日志对象
```java=
@Getter
@Setter
@Document
@ApiModel(value = "AdminAccessLogEntity", description = "后台操作日志")
public class AdminAccessLogEntity {

    @Id
    private String id;
    
    /**
     * 操作描述
     */
    @ApiModelProperty("操作描述")
    private String description;

    /**
     * 操作用户
     */
    @Indexed
    @ApiModelProperty("操作用户")
    private String username;

    /**
     * 操作时间
     */
    @ApiModelProperty("操作时间")
    private LocalDateTime startTime;

    /**
     * 消耗时间
     */
    @ApiModelProperty("消耗时间")
    private Integer spendTime;

    /**
     * 根路径
     */
    @ApiModelProperty("根路径")
    private String basePath;

    /**
     * URI
     */
    @ApiModelProperty("URI")
    private String uri;
    
    /**
     * 请求类型
     */
    @ApiModelProperty("请求类型")
    private String method;

    /**
     * IP地址
     */
    @ApiModelProperty("IP地址")
    private String ip;

    /**
     * 请求参数
     */
    @ApiModelProperty("请求参数")
    private Object parameter;

    /**
     * 请求返回的结果
     */
    @ApiModelProperty("请求返回的结果")
    private Object result;

    /**
     * 狀態
     */
    @ApiModelProperty("狀態")
    private boolean status;
}
```

### 创建Repository接口继承MongoRepository<T, ID>
```java=
public interface AdminAccessLogRepository extends MongoRepository<AdminAccessLogEntity, String> {

    /**
     * 分页查询操作日志
     * @param pageable 分页信息
     * @return Page<AdminAccessLogEntity>
     */
    Page<AdminAccessLogEntity> findAllByOrderByStartTimeDesc(Pageable pageable);

    /**
     * 分页查询操作日志
     * @param username 用户名
     * @param pageable 分页信息
     * @return Page<AdminAccessLogEntity>
     */
    Page<AdminAccessLogEntity> findByUsernameOrderByStartTimeDesc(String username, Pageable pageable);

    /**
     * 删除记录
     * @param id 主键
     */
    void removeAllById(String id);
}
```
### 创建操作日志Service
```java=
public interface AdminAccessLogService {

    /**
     * 分页查询操作日志
     * 
     * @param username 用户名
     * @param pageNum 页面
     * @param pageSize 每页数量
     * @return Page<AdminAccessLogEntity>
     */
    Page<AdminAccessLogEntity> getByPage(String username, Integer pageNum, Integer pageSize);

    /**
     * 新增一条操作日志
     * @param accessLog 对象
     * @return AdminAccessLogEntity
     */
    AdminAccessLogEntity save(AdminAccessLogEntity accessLog);

    /**
     * 删除一条操作日志
     * @param id 主键
     */
    void remove(String id);

    /**
     * 修改记录
     * @param accessLog 对象
     * @return AdminAccessLogEntity
     */
    AdminAccessLogEntity update(AdminAccessLogEntity accessLog);
}

```
```java=
@Service
public class AdminAccessLogServiceImpl implements AdminAccessLogService {

    @Resource
    private AdminAccessLogRepository accessLogRepository;

    @Override
    public Page<AdminAccessLogEntity> getByPage(String username, Integer pageNum, Integer pageSize) {
        var pageable = PageRequest.of(pageNum - 1, pageSize);
        return Optional.ofNullable(username)
                .map(v ->accessLogRepository.findByUsernameOrderByStartTimeDesc(v, pageable))
                .orElse(accessLogRepository.findAllByOrderByStartTimeDesc(pageable));
    }

    @Override
    public AdminAccessLogEntity save(AdminAccessLogEntity accessLog) {
        return accessLogRepository.insert@Slf4j
@Aspect
@Component
@Order(1)
public class WebsiteAccessLogAspect {

    @Resource
    private JwtTokenUtil jwtTokenUtil;
    @Resource
    private AdminAccessLogService accessLogService;
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Pointcut("execution(public * com.luke.xiaotiaowa.controller.*.*(..))")
    public void websiteAccessLogAspect() {
    }

    @Before("websiteAccessLogAspect()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
    }

    @AfterReturning(value = "websiteAccessLogAspect()", returning = "ret")
    public void doAfterReturning(Object ret) throws Throwable {
    }

    @Around("websiteAccessLogAspect()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        var now = LocalDateTime.now();
        //获取当前请求对象
        var attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        var request = attributes.getRequest();
        var signature = joinPoint.getSignature();
        var methodSignature = (MethodSignature) signature;
        var method = methodSignature.getMethod();
        // 返回结果
        Object result = null;
        // 日志对象
        var accessLog = buildAccessLog(request, method, joinPoint, now);
        try {
            result = joinPoint.proceed();
            accessLog.setResult(result);
        } catch (Exception e) {
            log.error("websiteAccessLogAspect err ", e);
            accessLog.setStatus(false);
        } finally {
            long endTime = System.currentTimeMillis();
            accessLog.setSpendTime((int) (endTime - startTime));
            try {
                accessLogService.save(accessLog);
            } catch (Exception e) {
                log.error("save mongoDB err ", e);
            }
        }
        log.info("{}", JSONUtil.parse(accessLog));
        return result;
    }
    
    /**
     * 解析请求参数
     * @param method 方法
     * @param args 入参
     * @return Object
     */
    private Object getParameter(Method method, Object[] args) {
        var argList = new ArrayList<>();
        var parameters = method.getParameters();
        for (var i = 0; i < parameters.length; i++) {
            var requestBody = parameters[i].getAnnotation(RequestBody.class);
            if (ObjectUtil.isNotNull(requestBody)) {
                argList.add(args[i]);
            }
            var requestParam = parameters[i].getAnnotation(RequestParam.class);
            if (ObjectUtil.isNotNull(requestParam)) {
                var map = new HashMap<String, Object>();
                var key = parameters[i].getName();
                if (StrUtil.isNotEmpty(requestParam.value())) {
                    key = requestParam.value();
                }
                map.put(key, args[i]);
                argList.add(map);
            }
        }
        return Optional.of(argList)
                .filter(v -> v.size() > 0)
                .map(v -> argList)
                .orElse(null);
    }
    
    private AdminAccessLog buildAccessLog(HttpServletRequest request, Method method, ProceedingJoinPoint joinPoint,
                                          LocalDateTime now) {
        var accessLog = new AdminAccessLog();
        var url = request.getRequestURL().toString();
        accessLog.setUsername(jwtTokenUtil.getUserNameFromToken(request.getHeader(this.tokenHeader).replace("Bearer ", "")));
        // 请求参数
        accessLog.setParameter(getParameter(method, joinPoint.getArgs()));
        if (method.isAnnotationPresent(ApiOperation.class)) {
            var apiOperation = method.getAnnotation(ApiOperation.class);
            accessLog.setDescription(apiOperation.value());
        }
        accessLog.setBasePath(StrUtil.removeSuffix(url, URLUtil.url(url).getPath()));
        accessLog.setIp(ServletUtil.getClientIP(request));
        accessLog.setMethod(request.getMethod());
        accessLog.setStartTime(now);
        accessLog.setUri(request.getRequestURI());
        accessLog.setStatus(true);
        return accessLog;
    }
}(accessLog);
    }

    @Override
    public void remove(String id) {
        accessLogRepository.deleteById(id);
    }

    @Override
    public AdminAccessLogEntity update(AdminAccessLogEntity accessLog) {
        return accessLogRepository.save(accessLog);
    }
}
```

### controller
```java=
@Api(tags = "系统管理")
@RestController
@RequestMapping("system")
public class SysAccessLogController {
    
    @Resource
    private AdminAccessLogService accessLogService;

    @ExclusionLog
    @ApiOperation("根据用户名或姓名分页获取用户列表")
    @GetMapping("admin")
    public Page<AdminAccessLogEntity> umsAdminPage(@ApiParam("用户名") @RequestParam(value = "username", required = false) String username,
                                                   @ApiParam("每页数量") @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                                   @ApiParam("页码") @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        return accessLogService.getByPage(username, pageNum, pageSize);
    }
}
```

## SpringBoot使用AOP
### 注解
| 注解            | 说明                                     |
| --------------- | ---------------------------------------- |
| @Aspect         | 用于定义切面                             |
| @Before         | 通知方法会在目标方法调用之前执行         |
| @After          | 通知方法会在目标方法返回或抛出异常后执行 |
| @AfterReturning | 通知方法会在目标方法返回后执行           |
| @AfterThrowing  | 通知方法会在目标方法抛出异常后执行       |
| @Around         | 通知方法会将目标方法封装起来             |
| @Pointcut       | 定义切点表达式                           |

### 引入依赖
```xml=
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```

### 创建AOP切面，实现功能
```java=
@Slf4j
@Aspect
@Component
@Order(1)
public class WebsiteAccessLogAspect {

    @Resource
    private JwtTokenUtil jwtTokenUtil;
    @Resource
    private AdminAccessLogService accessLogService;
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Pointcut("execution(public * com.luke.xiaotiaowa.controller.*.*(..))")
    public void websiteAccessLogAspect() {
    }

    @Before("websiteAccessLogAspect()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
    }

    @AfterReturning(value = "websiteAccessLogAspect()", returning = "ret")
    public void doAfterReturning(Object ret) throws Throwable {
    }

    @Around("websiteAccessLogAspect()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        var now = LocalDateTime.now();
        //获取当前请求对象
        var attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        var request = attributes.getRequest();
        var signature = joinPoint.getSignature();
        var methodSignature = (MethodSignature) signature;
        var method = methodSignature.getMethod();
        // 返回结果
        Object result = null;
        // 日志对象
        var accessLog = buildAccessLog(request, method, joinPoint, now);
        try {
            result = joinPoint.proceed();
            if (method.isAnnotationPresent(ExclusionLog.class)) {
                return result;
            }
            accessLog.setResult(result);
        } catch (Exception e) {
            log.error("websiteAccessLogAspect err ", e);
            accessLog.setStatus(false);
            throw e;
        } finally {
            long endTime = System.currentTimeMillis();
            accessLog.setSpendTime((int) (endTime - startTime));
            try {
                accessLogService.save(accessLog);
            } catch (Exception e) {
                log.error("save mongoDB err ", e);
            }
        }
        log.info("{}", JSONUtil.parse(accessLog));
        return result;
    }
    
    /**
     * 解析请求参数
     * @param method 方法
     * @param args 入参
     * @return Object
     */
    private Object getParameter(Method method, Object[] args) {
        var argList = new ArrayList<>();
        var parameters = method.getParameters();
        for (var i = 0; i < parameters.length; i++) {
            var requestBody = parameters[i].getAnnotation(RequestBody.class);
            if (ObjectUtil.isNotNull(requestBody)) {
                argList.add(args[i]);
            }
            var requestParam = parameters[i].getAnnotation(RequestParam.class);
            if (ObjectUtil.isNotNull(requestParam)) {
                var map = new HashMap<String, Object>();
                var key = parameters[i].getName();
                if (StrUtil.isNotEmpty(requestParam.value())) {
                    key = requestParam.value();
                }
                map.put(key, args[i]);
                argList.add(map);
            }
        }
        return Optional.of(argList)
                .filter(v -> v.size() > 0)
                .map(v -> argList)
                .orElse(null);
    }
    
    private AdminAccessLog buildAccessLog(HttpServletRequest request, Method method, ProceedingJoinPoint joinPoint,
                                          LocalDateTime now) {
        var accessLog = new AdminAccessLog();
        var url = request.getRequestURL().toString();
        accessLog.setUsername(jwtTokenUtil.getUserNameFromToken(request.getHeader(this.tokenHeader).replace("Bearer ", "")));
        // 请求参数
        accessLog.setParameter(getParameter(method, joinPoint.getArgs()));
        if (method.isAnnotationPresent(ApiOperation.class)) {
            var apiOperation = method.getAnnotation(ApiOperation.class);
            accessLog.setDescription(apiOperation.value());
        }
        accessLog.setBasePath(StrUtil.removeSuffix(url, URLUtil.url(url).getPath()));
        accessLog.setIp(ServletUtil.getClientIP(request));
        accessLog.setMethod(request.getMethod());
        accessLog.setStartTime(now);
        accessLog.setUri(request.getRequestURI());
        accessLog.setStatus(true);
        return accessLog;
    }
}
```

### 排除不需要记录日志的api
```java=
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ExclusionLog {
}
```

### 测试
```shell=
> use xiaotiaowa
switched to db xiaotiaowa
> show collections
adminAccessLog
> db.adminAccessLog.find({'description':'登录'})))))
{ "_id" : ObjectId("64253f83fe804f3de802c627"), "description" : "登录", "username" : "xiaotiaowa", "startTime" : ISODate("2023-03-30T07:51:31.354Z"), "spendTime" : 104, "basePath" : "http://localhost:8080", "uri" : "/auth", "method" : "POST", "ip" : "0:0:0:0:0:0:0:1", "parameter" : [ { "username" : "xiaotiaowa", "password" : "123456", "_class" : "com.luke.xiaotiaowa.vo.ums.UmsAdminLoginParam" } ], "result" : { "tokenHead" : "Bearer ", "token" : "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ4aWFvdGlhb3dhIiwiY3JlYXRlZCI6MTY4MDE2MjY5MTQ0MSwiZXhwIjoxNjgwNzY3NDkxfQ.hAoUnKpM8ftDEjneX4RmUaW_vjDTB077cJxSD_PWyOLdh2gN0AZVUhhdUuB8Ue9_fLXCOSRVtcwGxxiuW2YE2w" }, "status" : true, "_class" : "com.luke.xiaotiaowa.modules.system.entity.AdminAccessLog" }
>
```
> 写入登录日志成功
![输入图片说明](document/pic/accessLogPage.png)
![输入图片说明](document/pic/accessLogPage2.png)

