# SpringBoot+MySql+Mybatis-plus+Flyway搭建基本骨架
<!-- TOC -->
* [SpringBoot+MySql+Mybatis-plus+Flyway搭建基本骨架](#springbootmysqlmybatis-plusflyway搭建基本骨架)
  * [📚文档](#文档)
  * [依赖说明](#依赖说明)
  * [添加项目依赖](#添加项目依赖)
  * [SpringBoot](#springboot)
  * [修改application.yml配置文件](#修改applicationyml配置文件)
  * [mybatis-plus](#mybatis-plus)
    * [添加mybatis-plus的java config](#添加mybatis-plus的java-config)
    * [查询后台用户列表](#查询后台用户列表)
      * [xml](#xml)
      * [mapper](#mapper)
      * [service](#service)
      * [controller](#controller)
  * [Flyway](#flyway)
    * [添加Flyway的java config](#添加flyway的java-config)
    * [SpringBoot启动类](#springboot启动类)
    * [在resources创建放置sql的文件夹](#在resources创建放置sql的文件夹)
    * [在/db/migration/1.0创建sql](#在dbmigration10创建sql)
  * [Mybatis-plus generator](#mybatis-plus-generator-)
<!-- TOC -->

## 📚文档
[MyBatis-Plus指南](https://baomidou.com/pages/24112f/)

## 依赖说明
| 依赖包                    | 依赖版本 | 说明                      |
|------------------------| -------- | ------------------------- |
| SpringBoot             | 2.7.9    | Web应用开发框架           |
| MySql                  | 8.0.29   | 数据库                    |
| druid                  | 1.2.14   | 数据库连接池              |
| Mybatis-plus           | 3.5.2    | ORM框架,Mybatis的增强工具 |
| flyway                 | 7.9.1    | 数据库管理和迁移          |
| Mybatis-plus-generator | 3.5.2    | 代码生成器                |

## 添加项目依赖
```xml=
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
        <exclusions>
            <exclusion>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-tomcat</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-undertow</artifactId>
    </dependency>
    <!--Mysql数据库驱动-->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>${mysql-connector.version}</version>
    </dependency>
    <!--集成druid连接池-->
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>druid-spring-boot-starter</artifactId>
        <version>${druid.version}</version>
    </dependency>
    <!-- flyway数据库移植-->
    <dependency>
        <groupId>org.flywaydb</groupId>
        <artifactId>flyway-core</artifactId>
        <version>${flyway.version}</version>
    </dependency>
    <!-- mybatis-plus-->
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-boot-starter</artifactId>
        <version>${mybatis-plus.version}</version>
    </dependency>
</dependencies>
```

## SpringBoot
> 该样例使用内置的`undertow`的web容器，通过启动入口程序的main函数即可运行

## 修改application.yml配置文件
```yaml=
spring:
  datasource:
    username: "${db_username:root}"
    password: "${db_password:123456}"
    url: "${db_url}:jdbc:mysql://localhost:3306/xiaotiaowa?useSSL=false&serverTimezone=Asia/Shanghai&characterEncoding=utf-8"
    druid:
      initial-size: 5 #连接池初始化大小
      min-idle: 10 #最小空闲连接数
      max-active: 20 #最大连接数
      web-stat-filter:
        exclusions: "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*" #不统计这些请求数据
      stat-view-servlet: #访问监控网页的登录用户名和密码
        login-username: druid
        login-password: druid

mybatis-plus:
  configuration:
    map-underscore-to-camel-case: true #将下划线转换成驼峰开启
  mapper-locations:
    - classpath:mapper/**/*xml
  type-aliases-package: com/luke/xiaotiaowa
```

## mybatis-plus
### 添加mybatis-plus的java config
```java=
/**
 * mybatis-plus 配置
 * 
 * @author xiaotiaowa
 * @create 2023/3/22
 */
@Configuration
@EnableTransactionManagement
public class MybatisPlusConfig {

    /**
     * 分页插件
     * @return MybatisPlusInterceptor
     */
    @Bean
    public MybatisPlusInterceptor paginationInnerInterceptor() {
        var interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

    /**
     * 乐观锁插件
     * @return MybatisPlusInterceptor
     */
    @Bean
    public MybatisPlusInterceptor myBatisPlusVersionInterceptor() {
        var interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        return interceptor;
    }

    /**
     * 自动填充
     * @return MetaObjectHandler
     */
    @Bean
    public MetaObjectHandler metaObjectHandler() {
        return new MetaObjectHandler() {
            @Override
            public void insertFill(MetaObject metaObject) {
                var now = LocalDateTime.now();
                strictInsertFill(metaObject, "createTime", LocalDateTime.class, now);
                strictInsertFill(metaObject, "createdBy", String.class, "admin");
            }

            @Override
            public void updateFill(MetaObject metaObject) {
                var now = LocalDateTime.now();
                strictUpdateFill(metaObject, "lstModTime", LocalDateTime.class, now);
                strictUpdateFill(metaObject, "lstModBy", String.class, "admin");
            }
        };
    }
}
```

### 查询后台用户列表
#### xml
```java=
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.luke.xiaotiaowa.modules.ums.mapper.UmsAdminMapper">

    <sql id="Base_Column_List">
        id
        , username, password, icon, email, nick_name, note, login_time, status, version, 
            create_time, create_by, lst_mod_time, lst_mod_by
    </sql>

    <select id="selectListByParams" resultType="com.luke.xiaotiaowa.modules.ums.entity.UmsAdminEntity">
        select
        <include refid="Base_Column_List"/>
        from ums_admin
        <where>
            <if test="keyword != null and keyword != ''">
                username = #{keyword} or nick_name = #{keyword}
            </if>
        </where>
    </select>
</mapper>
```
#### mapper
```java=
@Mapper
public interface UmsAdminMapper extends BaseMapper<UmsAdminEntity> {

    /**
     * 后台用户列表
     * @param page 分页
     * @param keyword 用户名或昵称
     * @return list
     */
    List<UmsAdminEntity> selectListByParams(IPage<UmsAdminEntity> page, String keyword);
}
```
#### service
```java=
public interface UmsAdminService extends IService<UmsAdminEntity> {

    /**
     * 分页查询后台用户
     * @param page 分页
     * @param keyword 用户名或昵称
     * @return list
     */
    List<UmsAdminEntity> selectUmsAdmin(IPage<UmsAdminEntity> page, String keyword);
}

@Service
public class UmsAdminServiceImpl extends ServiceImpl<UmsAdminMapper, UmsAdminEntity> implements UmsAdminService {
    
    @Resource
    private UmsAdminMapper umsAdminMapper;

    @Override
    public List<UmsAdminEntity> selectUmsAdmin(IPage<UmsAdminEntity> page, String keyword) {
        return umsAdminMapper.selectListByParams(page, keyword);
    }
}
```

#### controller
```java=
/**
 * 用户列表
 *
 * @param keyword 参数
 * @return List<AuStaffModel>
 */
@ApiOperation("根据用户名或姓名分页获取用户列表")
@GetMapping
public List<UmsAdminEntity> umsAdminPage(@RequestParam(value = "keyword", required = false) String keyword,
                                         @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                         @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
    return umsAdminService.selectUmsAdmin(new Page<>(pageNum, pageSize, true), keyword);
}
```

## Flyway
### 添加Flyway的java config
```java=
@Slf4j
@Configuration
public class FlywayConfig {

    /**
     * 部署环境下，flyway 配置
     * @param dataSource 数据源
     * @return ClassicConfiguration
     */
    @Profile({ "dev", "prod" })
    @Bean
    public ClassicConfiguration flywayConfDev(DataSource dataSource) {
        log.info(">>> flyway profiles is not test");
        ClassicConfiguration cfg = new ClassicConfiguration();
        cfg.setDataSource(dataSource);
        cfg.setLocationsAsStrings("db/migration", "db/sampledata");
        return cfg;
    }

    @Profile({ "prod" })
    @Bean
    public ClassicConfiguration flywayConfProd(DataSource dataSource) {
        log.info(">>> flyway profiles is not test");
        ClassicConfiguration cfg = new ClassicConfiguration();
        cfg.setDataSource(dataSource);
        cfg.setLocationsAsStrings("db/migration");
        return cfg;
    }

    /**
     * 本地测试下，flyway 配置
     * @param dataSource 数据源
     * @return ClassicConfiguration
     */
    @Profile({ "test" })
    @Bean
    public ClassicConfiguration flywayConfTest(DataSource dataSource) {
        log.info(">>> flyway profiles is test");
        ClassicConfiguration cfg = new ClassicConfiguration();
        cfg.setDataSource(dataSource);
        cfg.setLocationsAsStrings("db/migration", "db/test");
        return cfg;
    }

    /**
     * flyway bean，这里为了安全，会删除 clean() 方法
     * @param cfg ClassicConfiguration
     * @return Flyway
     */
    @Bean
    public Flyway flyway(ClassicConfiguration cfg) {
        return new NonCleanFlyway(cfg);
    }
}
```
```java=
public class NonCleanFlyway extends Flyway {

    public NonCleanFlyway(Configuration configuration) {
        super(configuration);
    }

    @Override
    public CleanResult clean() {
        throw new RuntimeException("clean() is not allowed");
    }
}
```

### SpringBoot启动类
```java=
@Slf4j
@SpringBootApplication
public class XtwAdminApplication {

    public static void main(String[] args) {
        var run = SpringApplication.run(XtwAdminApplication.class, args);
        XtwAdminApplication.afterStartup(run);
    }

    /**
     * 启动后执行
     *
     * @param run ConfigurableApplicationContext
     */
    private static void afterStartup(ConfigurableApplicationContext run) {
        log.info(">>> run flyway");
        // 执行 flyway sql迭代
        var flyway = run.getBean(Flyway.class);
        flyway.baseline();
        flyway.migrate();
        log.info(">>> run flyway end");
    }
}
```

### 在resources创建放置sql的文件夹
> /resources/db/migration/1.0

### 在/db/migration/1.0创建sql
> 文件名格式：V1.0.年月日时分__操作_表名.sql
> 例：V1.0.202303221515__create_ums_admin.sql 
```sql=
CREATE TABLE `ums_admin`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录名',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `icon` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `nick_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `note` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `status` int(1) NULL DEFAULT 1 COMMENT '帐号启用状态：0->禁用；1->启用',
  `version` bigint NULL DEFAULT 0 COMMENT '乐观锁',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `create_by nvarchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `lst_mod_datetime datetime NULL COMMENT '最后修改人',
  `lst_mod_by nvarchar(50)  CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台管理员表' ROW_FORMAT = Dynamic;
```

## Mybatis-plus generator 
```java=
public class Genertor {

    /**
     * 数据源配置
     */
    private static final DataSourceConfig DATA_SOURCE_CONFIG = new DataSourceConfig
            .Builder("jdbc:mysql://localhost:3306/xiaotiaowa?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai",
            "root", "123456")
            .build();

    /**
     * 策略配置
     */
    public static StrategyConfig.Builder strategyConfig() {
        // 设置需要生成的表名
        return new StrategyConfig.Builder()
                .addInclude("ums_admin");
    }

    /**
     * 全局配置
     */
    protected static GlobalConfig.Builder globalConfig() {
        return new GlobalConfig.Builder().author("xiaotiaowa");
    }

    /**
     * 包配置
     */
    protected static PackageConfig.Builder packageConfig() {
        return new PackageConfig.Builder();
    }


    /**
     * 执行 run
     */
    public static void main(String[] args) {
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig().entityBuilder()
                .enableLombok()
                .superClass(BaseEntity.class)
                // 基于数据库字段
                .versionColumnName("version")
                // 基于模型属性
                .versionPropertyName("version")
                // 基于数据库字段
                .logicDeleteColumnName("deleted")
                // 基于模型属性
                .logicDeletePropertyName("deleteFlag")
                // 基于数据库字段
                .addIgnoreColumns("create_by", "create_time", "lst_mod_by", "lst_mod_time")
                // 重命名模板生成的文件名称
                .formatFileName("%sEntity")
                .mapperBuilder().formatMapperFileName("%sMapper").formatXmlFileName("%sMapper")
                .controllerBuilder().formatFileName("%sController")
                .serviceBuilder().formatServiceFileName("%sService").formatServiceImplFileName("%sServiceImpl")
                .build());
        generator.global(globalConfig()
                .enableSwagger()
                .build());
        generator.packageInfo(packageConfig()
                .parent("com.luke.xiaotiaowa.modules")
                .moduleName("ums").build());
        generator.execute();
    }
}
```
