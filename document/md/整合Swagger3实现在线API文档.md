# 整合Swagger3实现在线API文档
👉 https://swagger.io/ 👈

<!-- TOC -->
* [整合Swagger3实现在线API文档](#整合swagger3实现在线api文档)
  * [简介](#简介)
  * [常用注解](#常用注解)
  * [SpringBoot整合Swagger3](#springboot整合swagger3)
    * [添加依赖](#添加依赖)
    * [应用启动入口加上注解`@EnableOpenApi`](#应用启动入口加上注解enableopenapi)
    * [Swagger3Config的java config](#swagger3config的java-config)
    * [注解的使用](#注解的使用)
      * [entity层](#entity层)
      * [controller层](#controller层)
    * [访问 http://localhost:8080/swagger-ui/index.html](#访问-httplocalhost8080swagger-uiindexhtml)
<!-- TOC -->

## 简介
> Swagger是一个Restful风格接口的文档在线自动生成和测试的框架 

> Swagger是一套围绕OpenAPI规范构建的开源工具，可以帮助您设计、构建、记录和使用REST api。主要的Swagger工具包括:
* Swagger Editor -基于浏览器的编辑器，您可以在其中编写OpenAPI定义。
* Swagger UI -将OpenAPI定义呈现为交互式文档。
* Swagger Codegen -从OpenAPI定义生成服务器存根和客户端库。
* Swagger Editor Next (beta) -基于浏览器的编辑器，您可以在其中编写和查看OpenAPI和AsyncAPI定义。
* Swagger Core——与java相关的库，用于创建、使用和使用OpenAPI定义。
* Swagger Parser -用于解析OpenAPI定义的独立库
* Swagger APIDom——提供了一个单一的、统一的结构，用于跨各种描述语言和序列化格式描述api。

## 常用注解
* @Api：用于修饰Controller类，生成Controller相关文档信息
* @ApiOperation：用于修饰Controller类中的方法，生成接口方法相关文档信息
* @ApiParam：用于修饰接口中的参数，生成接口参数相关文档信息
* @ApiModelProperty：用于修饰实体类的属性，当实体类是请求参数或返回结果时，直接生成相关文档信息

## SpringBoot整合Swagger3
### 添加依赖
```xml=
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-boot-starter</artifactId>
    <version>3.0.0</version>
</dependency>
```

### 应用启动入口加上注解`@EnableOpenApi`
```java=
@Slf4j
@EnableOpenApi
@SpringBootApplication
public class XtwAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(XtwAdminApplication.class, args);
    }
```

### Swagger3Config的java config
```java=
@Configuration
public class Swagger3Config {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("小跳蛙系统的Swagger3接口文档")
                .description("小跳蛙")
                .contact(new Contact("Ray。", "", "894131102@qq.com"))
                .version("1.0")
                .build();
    }
    
    /**
     * 增加如下配置可解决Spring Boot版本 与Swagger 3.0.0 不兼容问题
     **/
    @Bean
    public WebMvcEndpointHandlerMapping webEndpointServletHandlerMapping(WebEndpointsSupplier webEndpointsSupplier,
                                                                         ServletEndpointsSupplier servletEndpointsSupplier,
                                                                         ControllerEndpointsSupplier controllerEndpointsSupplier,
                                                                         EndpointMediaTypes endpointMediaTypes,
                                                                         CorsEndpointProperties corsProperties,
                                                                         WebEndpointProperties webEndpointProperties, 
                                                                         Environment environment) {
        var allEndpoints = new ArrayList<ExposableEndpoint<?>>();
        var webEndpoints = webEndpointsSupplier.getEndpoints();
        allEndpoints.addAll(webEndpoints);
        allEndpoints.addAll(servletEndpointsSupplier.getEndpoints());
        allEndpoints.addAll(controllerEndpointsSupplier.getEndpoints());
        var basePath = webEndpointProperties.getBasePath();
        var endpointMapping = new EndpointMapping(basePath);
        boolean shouldRegisterLinksMapping = this.shouldRegisterLinksMapping(webEndpointProperties, environment, basePath);
        return new WebMvcEndpointHandlerMapping(endpointMapping, webEndpoints, endpointMediaTypes,
                corsProperties.toCorsConfiguration(), new EndpointLinksResolver(allEndpoints, basePath), 
                shouldRegisterLinksMapping, null);
    }

    private boolean shouldRegisterLinksMapping(WebEndpointProperties webEndpointProperties, Environment environment, String basePath) {
        return webEndpointProperties.getDiscovery().isEnabled() 
                && (StringUtils.hasText(basePath) 
                || ManagementPortType.get(environment).equals(ManagementPortType.DIFFERENT));
    }
}
```

### 注解的使用
#### entity层
```java=
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("ums_admin")
@ApiModel(value = "UmsAdminEntity对象", description = "后台管理员表")
public class UmsAdminEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("登录名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("头像")
    private String icon;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("备注信息")
    private String note;

    @ApiModelProperty("最后登录时间")
    private LocalDateTime loginTime;

    @ApiModelProperty("帐号启用状态：0->禁用；1->启用")
    private Integer status;

    @ApiModelProperty("乐观锁")
    @Version
    private Long version;
}
```

#### controller层
```java=
@Api(tags = "后台用户管理")
@RestController
@RequestMapping("/ums/ums_admin")
public class UmsAdminController {
    
    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @ApiOperation("通过主键查询单条数据")
    @GetMapping("{id}")
    public UmsAdminEntity umsAdmin(@PathVariable long id) {
        return new UmsAdminEntity();
    }
}
```

### 访问 http://localhost:8080/swagger-ui/index.html
![image](https://user-images.githubusercontent.com/42085809/226921439-c3a48cdc-26e5-40ef-b076-93acb7db4d08.png)
